Remember to add FLS for the 'Used For Votes' fields in Organization Structure and Region objects.

**IMPORTANT**
As of Winter '20, Salesforce has a setting that must be enabled to have an attached File inherit the parent record's access.
Files -> General Settings -> Enable Set file access to Set By Record