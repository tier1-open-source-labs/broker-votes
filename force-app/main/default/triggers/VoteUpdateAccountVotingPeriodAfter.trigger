/** 
 * Name:  VoteUpdateAccountVotingPeriodAfter
 * Description: Updates the Account Voting Period with the Global non-LOB Values
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
trigger VoteUpdateAccountVotingPeriodAfter on Vote__c (after delete, after insert, after undelete, after update)
{
    Id globalRegionId;
    map<Id, Vote__c> globalVoteMap = new map<Id, Vote__c>();
    list<T1C_Base__Account_Voting_Period__c> avpsToUpdate = new list<T1C_Base__Account_Voting_Period__c>();
    
    for(T1C_Base__Region__c reg : [Select Id, Name from T1C_Base__Region__c where Name = 'Global'])
    {
        globalRegionId = reg.Id;
    }
    
    if(globalRegionId == null)
    {
        system.debug('No Global Region Found');
        return;
    }
    
    for(Vote__c vote : trigger.isDelete ? trigger.old : trigger.new)
    {
        if(vote.LOB__c == null && vote.Region__c == globalRegionId)
        {
            globalVoteMap.put(vote.Account_Voting_Period__c, vote);
        }
    }
    
    if(globalVoteMap.isEmpty())
    {
        system.debug('No Global Votes Found');
        return;
    }
    
    for(T1C_Base__Account_Voting_Period__c avp : [Select Global_Rank__c, Rank_Out_Of__c, Global_Tier__c, Tier_Out_Of__c, Id from T1C_Base__Account_Voting_Period__c where Id = :globalVoteMap.keySet()])
    {
        Vote__c v = globalVoteMap.get(avp.Id);
        if(v == null)
        {
            continue;
        }
        
        avp.Global_Rank__c = v.Rank__c;
        avp.Rank_Out_Of__c = v.Rank_Out_Of__c;
        avp.Global_Tier__c = v.Tier__c;
        avp.Tier_Out_Of__c = v.Tier_Out_Of__c;
        
        avpsToUpdate.add(avp);
    }
    
    if(!avpsToUpdate.isEmpty())
    {
        update avpsToUpdate;
    }
}