/*
 * Name:        VotesBeforeSave
 *
 * Description: Trigger BEFORE Vote creation/update to update Vote.Account from Vote.Contact.Account
 *
 * Notes: 		
 *
 * Confidential & Proprietary, 2014 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1CRM.
 */
trigger VotesBeforeSave on Vote__c (before insert, before update) 
{
	//check if the trigger is active
    //if it is not, abort
	if(T1C_Base.ACETriggerCheck.isDisabled('VotesBeforeSave'))
    {
    	return;
    }
    
    set<Id> contIds = new set<Id>{};
    for(Vote__c v : trigger.new)
    {
    	if(v.Contact__c != null)
    	{
    		contIds.add(v.Contact__c);
    	}
    }
    
    map<Id,Contact> contacts = new map<Id,Contact>([select Id, AccountId from Contact where Id IN: contIds]);
    
    for(Vote__c v : trigger.new)
    {
    	if(v.Contact__c != null)
    	{
    		Id acctId = contacts.get(v.Contact__c).AccountId;
    		v.Account_of_Voter__c = acctId;
    	}
    }
}