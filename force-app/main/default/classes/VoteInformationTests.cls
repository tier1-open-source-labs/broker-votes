/** 
 * Name:  VoteInformationTests
 * Description: Test Class for the VoteInformationController
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
@isTest
private class VoteInformationTests 
{	
	private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();
	static Account acct;
    static list<T1C_Base__Region__c> regions;
	static T1C_Base__Account_Voting_Period__c avp;
   	static T1C_Base__Team__c globalTeam;
	static T1C_Base__Team__c asiaTeam;
	static T1C_Base__Team__c euTeam;
    static T1C_Base__Team__c usTeam;
    static list<T1C_Base__Employee__c> emps;

	static void setupData()
	{
		T1C_FR__Feature__c ace = new T1C_FR__Feature__c(Name = 'ACE',T1C_FR__Name__c = 'ACE');
		T1C_FR__Feature__c cw = new T1C_FR__Feature__c(Name = 'CoreWrappers',T1C_FR__Name__c = 'ACE.CoreWrappers');
		T1C_FR__Feature__c votes = new T1C_FR__Feature__c(Name = 'Votes',T1C_FR__Name__c = 'ACE.CoreWrappers.Votes');
        T1C_FR__Feature__c targetRankDefault = new T1C_FR__Feature__c(Name = 'TargetRankDefault', T1C_FR__Name__c = 'ACE.CoreWrappers.Votes.TargetRankDefault');
        T1C_FR__Feature__c proactive = new T1C_FR__Feature__c(Name = 'Proactive', T1C_FR__Name__c = 'ACE.CoreWrappers.Votes.TargetRankDefault.Proactive');
        T1C_FR__Feature__c points = new T1C_FR__Feature__c(Name = 'Points',T1C_FR__Name__c = 'ACE.CoreWrappers.Votes.Points');
        T1C_FR__Feature__c rank = new T1C_FR__Feature__c(Name = 'Rank', T1C_FR__Name__c = 'ACE.CoreWrappers.Votes.Points.Rank');
		T1C_FR__Feature__c belowTopTen = new T1C_FR__Feature__c(Name = 'BelowTopTen', T1C_FR__Name__c = 'ACE.CoreWrappers.Votes.Points.Rank.BelowTopTen');
        T1C_FR__Feature__c tier = new T1C_FR__Feature__c(Name = 'Tier', T1C_FR__Name__c = 'ACE.CoreWrappers.Votes.Points.Tier');
        T1C_FR__Feature__c focusGold = new T1C_FR__Feature__c(Name = 'FocusGold', T1C_FR__Name__c = 'ACE.CoreWrappers.Votes.Points.Tier.FocusGold');
		
		upsert new list<T1C_FR__Feature__c>{ace,cw,votes,targetRankDefault,proactive,points,rank,belowTopTen, tier, focusGold};
            
        insert new list<T1C_FR__Attribute__c>
        {    
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = Votes.Id, Name = 'CanDelete', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = Votes.Id, Name = 'CanEdit', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = Votes.Id, Name = 'Regions', T1C_FR__Value__c = 'All'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = Votes.Id, Name = 'VoteSeries', T1C_FR__Value__c = 'Overall,Overall Rank,Research'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = Votes.Id, Name = 'EnhancedLinesOfBusinesses', T1C_FR__Value__c = 'Sales;Research'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = Votes.Id, Name = 'FilteredOutLinesOfBusinesses', T1C_FR__Value__c = 'Event'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = Votes.Id, Name = 'GetInterestSubjectCoverage', T1C_FR__Value__c = 'true'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = Votes.Id, Name = 'LOBsToSeparateByTeam', T1C_FR__Value__c = 'Sales'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = Proactive.Id, Name = 'TargetRankDefaultValue', T1C_FR__Value__c = 'Top 3'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = Points.Id, Name = 'Enabled', T1C_FR__Value__c = 'TRUE'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = BelowTopTen.Id, Name = 'Name', T1C_FR__Value__c = '>Top 10'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = BelowTopTen.Id, Name = 'Points', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = FocusGold.Id, Name = 'Name', T1C_FR__Value__c = 'Focus - Gold'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = FocusGold.Id, Name = 'Points', T1C_FR__Value__c = '6')
        }; 
		
		acct = dataFactory.makeAccount('Test Account');
        insert acct;
        
        regions = new list<T1C_Base__Region__c>
        {
            new T1C_Base__Region__c(Name = 'Global', Used_for_Votes__c = true),
            new T1C_Base__Region__c(Name = 'Asia', Used_for_Votes__c = true),
			new T1C_Base__Region__c(Name = 'Euro', Used_for_Votes__c = true),
            new T1C_Base__Region__c(Name = 'US', Used_for_Votes__c = true)
        };
        insert regions;
        
        globalTeam = new T1C_Base__Team__c(Name = 'Global Team', Is_Vote_Team__c = true, Region__c = regions[0].Id);
		asiaTeam = new T1C_Base__Team__c(Name = 'Asia Team', Is_Vote_Team__c = true, Region__c = regions[1].Id);
		euTeam = new T1C_Base__Team__c(Name = 'Eu Team', Is_Vote_Team__c = true, Region__c = regions[2].Id);
        usTeam = new T1C_Base__Team__c(Name = 'US Team', Is_Vote_Team__c = true, Region__c = regions[3].Id);
        
        list<T1C_Base__Organization_Structure__c> lobs = new list<T1C_Base__Organization_Structure__c>
        {
        	dataFactory.makeOrgStructureWithLOBRT('Sales'),
        	dataFactory.makeOrgStructureWithLOBRT('Research'),
        	dataFactory.makeOrgStructureWithLOBRT('Event'),
        	dataFactory.makeOrgStructureWithLOBRT('Trading')        	
        };
        lobs[0].Used_for_Votes__c = true; 
		lobs[1].Used_for_Votes__c = true; 
		lobs[2].Used_for_Votes__c = true; 
		lobs[3].Used_for_Votes__c = true;

        insert lobs;
        
        T1C_Base__Employee__c emp1 = dataFactory.makeEmployee('Johnny','Boy',null);
		emp1.T1C_Base__LOB__c = lobs[0].Id;
		
		T1C_Base__Employee__c emp2 = dataFactory.makeEmployee('Super','Analyst',null);
		emp2.T1C_Base__LOB__c = lobs[1].Id;
		
		T1C_Base__Employee__c emp3 = dataFactory.makeEmployee('Some','Trader',null);
		emp3.T1C_Base__LOB__c = lobs[2].Id;
		
		T1C_Base__Employee__c emp4 = dataFactory.makeEmployee('Mitch','Conner',null);
		emp4.T1C_Base__LOB__c = lobs[3].Id;
        
		T1C_Base__Employee__c emp5 = dataFactory.makeEmployee('Ivan','Drago',null);
		emp5.T1C_Base__LOB__c = lobs[0].Id;
        
        emps = new list<T1C_Base__Employee__c>{emp1,emp2,emp3,emp4,emp5};        
        insert emps;
        
		T1C_Base__Interest_Subject_Type__c ist = dataFactory.makeInterestSubjectType('Tickers',null);
        insert ist;
        
        T1C_Base__Interest_Subject__c is = dataFactory.makeInterestSubject(ist.Id,'GOOG',null);
        insert is;
        
        T1C_Base__Interest_Subject_Coverage__c isc1 = new T1C_Base__Interest_Subject_Coverage__c(T1C_Base__Employee__c = emp1.Id, T1C_Base__Interest_Subject__c = is.Id, Region__c = regions[0].Id);
        T1C_Base__Interest_Subject_Coverage__c isc2 = new T1C_Base__Interest_Subject_Coverage__c(T1C_Base__Employee__c = emp2.Id, T1C_Base__Interest_Subject__c = is.Id, Region__c = regions[1].Id);
        T1C_Base__Interest_Subject_Coverage__c isc3 = new T1C_Base__Interest_Subject_Coverage__c(T1C_Base__Employee__c = emp3.Id, T1C_Base__Interest_Subject__c = is.Id, Region__c = regions[2].Id);
        T1C_Base__Interest_Subject_Coverage__c isc4 = new T1C_Base__Interest_Subject_Coverage__c(T1C_Base__Employee__c = emp4.Id, T1C_Base__Interest_Subject__c = is.Id, Region__c = regions[3].Id);

        insert new List<T1C_Base__Interest_Subject_Coverage__c>
        {
            isc1,
            isc2,
            isc3,
            isc4
        };
        
        list<T1C_Base__Account_Coverage__c> acs = new list<T1C_Base__Account_Coverage__c>
        {
			dataFactory.makeAccountCoverage(acct.Id,emp1.Id),
			dataFactory.makeAccountCoverage(acct.Id,emp3.Id),
			dataFactory.makeAccountCoverage(acct.Id,emp4.Id),
			dataFactory.makeAccountCoverage(acct.Id,emp5.Id)
        };
        insert acs;
        
        T1C_Base__Account_Coverage_Additional_Info__c acai = new T1C_Base__Account_Coverage_Additional_Info__c(T1C_Base__Account_Coverage__c = acs[0].Id);
		insert acai;
        
        ContentVersion cv = new ContentVersion(
            Title = 'Test Content Version',
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
    	);
    	insert cv;
	}
	
    static testMethod void voteInitTest() 
    {
    	setupData();
        
		// VoteInformationController.AccountVoteInfo avi = VoteInformationController.getVoteInfo(null, acct.Id);
		VoteInformationController.AccountVoteInfo avi = VoteInformationController.getVoteInfo(null, acct.Id, UserInfo.getUserId());
        
        avp = avi.acctVote;
        avp.T1C_Base__Region__c = regions[0].Id;
        insert avp;
        
        Contact contact = dataFactory.makeContact(acct.Id, 'Contact', 'Test');
        insert contact;
        
        Vote__c vote = new Vote__c(Account_Voting_Period__c = avp.Id, Team__c = globalTeam.Id, Contact__c = contact.Id, Region__c = regions[0].Id);
        insert vote;
        
        list<Vote__c> votes = new list<Vote__c>();
        system.debug(avi);
        for(VoteInformationController.RegionVoteInfo allLob : avi.accountVoteInfo.regionVotes)
        {
        	Vote__c v = allLob.vote;
    		v.Account_Voting_Period__c = avp.Id;
    		votes.add(v);
        }
        
        for(VoteInformationController.LOBVoteInfo lvi : avi.lobVoteInfos)
        {
        	for(VoteInformationController.RegionVoteInfo rv : lvi.regionVotes)
        	{
        		Vote__c v = rv.vote;
        		v.Account_Voting_Period__c = avp.Id;
        		votes.add(v);
        		
        		for(VoteInformationController.VoteInfo ev : rv.employeeVotes)
        		{
        			Vote__c empVC = ev.vote;
        			empVC.Account_Voting_Period__c = avp.Id;
        			votes.add(empVC);
        		}
        	}
        }
        insert votes;
        
        list<VoteInformationController.VoteInfo> voteInfos = new list<VoteInformationController.VoteInfo>();
        voteInfos.add(new VoteInformationController.VoteInfo(new Vote__c(Account_Voting_Period__c = avp.Id, Team__c = globalTeam.Id),globalTeam));
        voteInfos.add(new VoteInformationController.VoteInfo(new Vote__c(Account_Voting_Period__c = avp.Id, Team__c = euTeam.Id),euTeam));
		voteInfos.sort();
        
        list<VoteInformationController.TeamVoteInfo> teamVoteInfos = new list<VoteInformationController.TeamVoteInfo>();
        teamVoteInfos.add(new VoteInformationController.TeamVoteInfo('Test Team 1'));
        teamVoteInfos.add(new VoteInformationController.TeamVoteInfo('Test Team 2'));
		teamVoteInfos.sort();
        
        for(T1C_Base__Employee__c emp : emps)
        {
            voteInfos.add(new VoteInformationController.VoteInfo(new Vote__c(Account_Voting_Period__c = avp.Id, Employee__c = emp.Id),emp));
        }
        
        VoteInformationController.ReturnResult result = VoteInformationController.saveVotes(avp, voteInfos, new list<Id>{});

        
		// avi = VoteInformationController.getVoteInfo(avp.Id, acct.Id);
		avi = VoteInformationController.getVoteInfo(avp.Id, acct.Id, UserInfo.getUserId());
        Blob attBody = Blob.valueOf('1234567890');
        Attachment att = new Attachment(Name = 'test attachment', Body = attBody, ParentId = avp.Id);
        insert att;
        
        delete att;
        
        list<ContentDocumentLink> atts = VoteInformationController.getAttachmentsForGrid(avp.Id);
                
        List<ContentDocument> cdoc = [SELECT Id FROM ContentDocument];
        VoteInformationController.reparentFile(cdoc[0].Id, acct.Id, null);
    }
}