(function($)
{	
	var lastVoteThis;
	var lastNotesInput;
	var votesWrapper         = $('<div class="votesWrapper" />').appendTo("body");	 
	var votesHeaderContainer = $('<div class="votesHeader" />').appendTo(votesWrapper);
	var votesHeader          = $('<div class="votesHeaderLabel">Voting Details</div>').appendTo(votesHeaderContainer);	
	var votesContentWrapper  = $('<div class="votesContentWrapper"/>').appendTo(votesWrapper);
	var formBody             = $('<div class="votesBody" />').appendTo(votesContentWrapper);
	var footerContainer      = $('<div class="votesFooter" />').appendTo(votesWrapper);
	var footer 		         = $('<div class="footerButtonWrapper" />').appendTo(footerContainer);
	
	var modalThisNotesContainer;
	var modalThisContactButton;

	var dialog = $('<div></div>').dialog({resizable : false, autoOpen: false, dialogClass: null, height: "auto", buttons: [], modal: true,createOnShow: false, title: 'Add Notes and Contacts'});
	var dialogParent = $('<div class="dialog-main-content el-block"/>').appendTo(dialog);
	var dialogAppendTo = $('<div class="el-block flex-parent is-vertical">').appendTo(dialogParent);
	$(".ui-dialog").addClass("progressBarDialog aceDialog update-team-info-dialog el-dialog");

	var modalParent = $('<div class="el-block flex-parent is-vertical"/>').appendTo(dialogAppendTo);
		$('<h3 class="">Add Notes</h3>').appendTo(modalParent);
		$('<p class="el-message">Add Notes to this vote</p>').appendTo(modalParent);
	var notesInputDiv = $('<div class="vote-notes"/>').appendTo(modalParent);

	var contactsParent = $('<div class="el-block flex-parent is-vertical"/>').appendTo(dialogAppendTo);
		$('<h3 class="el-section-title">Add Account Contact(s)</h3>').appendTo(contactsParent);
		$('<p class="el-message">Search for Account Contacts Below to add them.</p>').appendTo(contactsParent);
	
	var modalAccountContactsParent = $('<div class="el-block flex-parent is-vertical"/>').appendTo(contactsParent);		
	var modalAccountContactsFlexParent = $('<div class="el-row flex-parent"/>').appendTo(modalAccountContactsParent);
	var modalAccountContactsItemWrapper = $('<div class="el-item-wrapper flex-parent is-vertical">').appendTo(modalAccountContactsFlexParent);
	var contactLookupAutoCompleteDiv = $('<div class="el-item el-block search-wrapper"/>').appendTo(modalAccountContactsItemWrapper);
	
	var contactLookupAutoCompleteDataGridDiv = $('<div style="width:100%;height:100px"/>').appendTo(modalAccountContactsItemWrapper);
		
	var contactLookupAutoCompleteDataGrid = new ACEDataGrid($(contactLookupAutoCompleteDataGridDiv) ,{  maxNumberOfRows:7, 
		autoHeight: true, 
		forceFitColumns: true, 
		explicitInitialization: true,
		enableAutoResize: true,
		editable: false,																					
		headerRowHeight: 40
	});

	contactLookupAutoCompleteDataGrid.setColumns([{id: 'contactName', field: 'Name', width: 100, name: 'Name', sortable: true, fieldType: 'String'},
												{id: 'voteContactId', resizable: false, field: 'id', maxWidth: 40, name: '', sortable: true, fieldType: 'String', formatter : deleteButton}]);
	
	contactLookupAutoCompleteDataGrid.grid.onClick.subscribe(function(e, args) {
		var cell = args.cell;
		if(cell == 1)
		{
			var newItems = [];
			var delItem = args.grid.getDataItem(args.row);
			
			var dataGridItems = contactLookupAutoCompleteDataGrid.grid.getData().getItems();

			for(var x=0;x<dataGridItems.length;x++)
			{
				var thisItem = dataGridItems[x];
				if(delItem != thisItem)
				{
					newItems.push(thisItem);
				}
				else if(thisItem.contactId != thisItem.id)
				{
					delContacts.push(thisItem.id);
				}
			}

			setGridData(newItems);
		}	
	});

	var modalfooter = $('<footer class="dialog-footer"/>').appendTo(dialog);
	var modalfooterWrapper = $('<div class="footerButtonWrapper"/>').appendTo(modalfooter);
	var modalfooterConfirmButton = $('<button class="confirmButton">Save</button>').appendTo(modalfooterWrapper);
	var modalfooterCancelButton = $('<button class="cancelButton">Cancel</button>').appendTo(modalfooterWrapper);
	
	var newNote = window.location.search.search('AccountId') !== -1 ?  true: false; 
	var sessionId = ACE.Salesforce.sessionId;
	var serverURL = ACE.Salesforce.serverUrl;
	serverURL = serverURL.substr(0,serverURL.indexOf("/services"));
	
	function deleteButton(row, cell, value, columnDef, dataContext)
	{
		return  '<span data-type="deleteButton"></span>';
	}

	var initGrid = true;
	var delContacts = [];
	sforce.connection.sessionId = ACE.Salesforce.sessionId;
	var dateTimeZoneOffset = new Date().getTimezoneOffset() * 60000;		   
	var accountVotingPeriodToUpdate;
	var saveButton;
	var fileMap = {};
	var attachmentCount = 0;
	var attachmentParent;
	var lastAttachmentId;
	var lastAttachmentName;
	var restrictions = null;
	var accountVotingPeriod;
	var allLOBVote;
	var periodSelected = 'Select One';
	var accountName;
	var accountLogo;
	var LOBVotes;
	var regionVotes;
	var years;
	var loadStyleSheets = true;	
	var zipURL;
	var cssFiles;
	var startDate;
	var endDate;
	var lastReceived;
	var period;
	var periodDataProvider = [];
	var startOfYear = new Date(new Date().getFullYear(),0,1,0,0);
	var gridInit = false;
	var attatColumns = [];
	var acctVoteInfo;
	var rankPicklistValues = [];
	var targetRankPicklistValues = [];
	var rankPicklistMaster;
	var targetRankPicklistMaster;
	var voteWrappers = [];
	var votes = [];
	var teamSelected = true;
	var userId = ACEUtil.getURLParameter('User');
	var defaultLOB;
	var regionValue;
	var rankPointsMap = {};
	var tierMultipliers = [];
	var tierMultiplierMap = {};
	var tempVoteWrappers = [];
	var enhancedLOBHasChanges = false; 
	var nonLOBSectionHasChanges = false; 

	if(userId == null)
	{
		userId = ACE.Salesforce.userId;
	}

	ACE.AML.Parser.APEXImporter.import(ACE.Salesforce.baseURL + '/apex/VoteInformationControllerPage').then(function(e,args)
	{
		ACE.Remoting.call("VoteInformationController.getVoteInfo", [ACEUtil.getURLParameter('Id'), ACEUtil.getURLParameter('AccountId'), userId], function(result,event)
		{ 
			if(event.status)
			{			
				acctVoteInfo = result;

				console.log(acctVoteInfo);

				if (acctVoteInfo.rankMapping != null)
				{
					for(var x=0;x<acctVoteInfo.rankMapping.length;x++)
					{
						rankPointsMap[acctVoteInfo.rankMapping[x].name] = acctVoteInfo.rankMapping[x].points;
					}
				}
				
				
				acctVoteInfo.rankPointsMap = rankPointsMap;

				tierMultipliers = acctVoteInfo.pointMapping;

				if (acctVoteInfo.pointMapping != null)
				{
					for(var x=0;x<acctVoteInfo.pointMapping.lengh;x++)
					{
						tierMultiplierMap[acctVoteInfo.pointMapping[x].name] = acctVoteInfo.pointMapping[x];
					}
				}

				defaultLOB = acctVoteInfo.defaultLOB;

				restrictions = acctVoteInfo.restrictions;

				accountVotingPeriodToUpdate = acctVoteInfo.acctVote;
				
				if(acctVoteInfo.acctVote.Id !== undefined && acctVoteInfo.acctVote.Id != null)
				{
					accountVotingPeriodToUpdate.Id = acctVoteInfo.acctVote.Id;
				}
				
				accountVotingPeriod = acctVoteInfo.acctVote;
				acctVoteInfo.accountVoteInfo.voteSeriesPicklist = acctVoteInfo.voteSeriesPicklist;

				allLOBVote = acctVoteInfo.accountVoteInfo;
				accountName = acctVoteInfo.accountName;
				accountLogo = acctVoteInfo.accountLogo;
				years = acctVoteInfo.years;
				LOBVotes = acctVoteInfo.lobVoteInfos;

				if(acctVoteInfo.rankPicklistValues != null)
				{
					rankPicklistValues.push({label:"",data:"",defaultSelected:true});

					for(var x=0;x<acctVoteInfo.rankPicklistValues.length;x++)
					{
						var plv = acctVoteInfo.rankPicklistValues[x];

						rankPicklistValues.push({label:plv.label,data:plv.value,defaultSelected:false});
					}
				}
				
				rankPicklistMaster = createEnhancedVotePicklist(rankPicklistValues);

				if(acctVoteInfo.targetPicklistValues != null)
				{
					targetRankPicklistValues.push({label:"",data:"",defaultSelected:true});

					for(var x=0;x<acctVoteInfo.targetPicklistValues.length;x++)
					{
						var plv = acctVoteInfo.targetPicklistValues[x];

						targetRankPicklistValues.push({label:plv.label,data:plv.value,defaultSelected:false});
					}
				}

				targetRankPicklistMaster = createEnhancedVotePicklist(targetRankPicklistValues);

				generateHTML();
			}
			else
			{
				console.log(result);
				console.log(event);
			} 
		});
		
		//  ==================================================================
		//  Method         : validatDateFields
		//  Description    : set date times 
		//  @param         : setToCustom, periodVal, period
		//  ==================================================================
		function validatDateFields(setToCustom, periodVal, period, isInit)
		{		
			if(setToCustom)
			{
				period[0].menuButton.setSelectedItem('Custom');			
				periodVal = 'Custom';
			}		
			else
			{
				if (!isInit)
				{
					if(periodVal == 'Q1')
					{
						$(startDate).val(new Date(startOfYear).toLocaleDateString());
						$(startDate).datepicker("setDate", new Date($(startDate).val()));
						$(endDate).val(new Date(new Date().getFullYear(),2,31,0,0).toLocaleDateString());
						$(endDate).datepicker("setDate", new Date($(endDate).val()));
					}
					else if(periodVal == 'Q2')
					{
						$(startDate).val(new Date(new Date().getFullYear(),3,1).toLocaleDateString());
						$(startDate).datepicker("setDate", new Date($(startDate).val()));
						$(endDate).val(new Date(new Date().getFullYear(),5,30,0,0).toLocaleDateString());
						$(endDate).datepicker("setDate", new Date($(endDate).val()));
					}
					else if(periodVal == 'Q3')
					{
						$(startDate).val(new Date(new Date().getFullYear(),6,1).toLocaleDateString());
						$(startDate).datepicker("setDate", new Date($(startDate).val()));
						$(endDate).val(new Date(new Date().getFullYear(),8,30,0,0).toLocaleDateString());
						$(endDate).datepicker("setDate", new Date($(endDate).val()));
					}
					else if(periodVal == 'Q4')
					{
						$(startDate).val(new Date(new Date().getFullYear(),9,1).toLocaleDateString());
						$(startDate).datepicker("setDate", new Date($(startDate).val()));
						$(endDate).val(new Date(new Date().getFullYear(),11,31,0,0).toLocaleDateString());
						$(endDate).datepicker("setDate", new Date($(endDate).val()));
					}
					if(periodVal == 'H1')
					{
						$(startDate).val(new Date(startOfYear).toLocaleDateString());
						$(startDate).datepicker("setDate", new Date($(startDate).val()));
						$(endDate).val(new Date(new Date().getFullYear(),5,30,0,0).toLocaleDateString());
						$(endDate).datepicker("setDate", new Date($(endDate).val()));
					}
					else if(periodVal == 'H2')
					{
						$(startDate).val(new Date(new Date().getFullYear(),6,1).toLocaleDateString());
						$(startDate).datepicker("setDate", new Date($(startDate).val()));
						$(endDate).val(new Date(new Date().getFullYear(),11,31,0,0).toLocaleDateString());
						$(endDate).datepicker("setDate", new Date($(endDate).val()));
					}
				}
				
				accountVotingPeriodToUpdate.Start_Date__c = new Date($(startDate).val()) - new Date($(startDate).val()).getTimezoneOffset() * 60000;
				accountVotingPeriodToUpdate.End_Date__c = new Date($(endDate).val()) - new Date($(endDate).val()).getTimezoneOffset() * 60000;
			}
			
			periodSelected = periodVal;
		}
		
		function checkDateRange()
		{
			var startVal = $(startDate).val();
			var endVal = $(endDate).val();
			if(startVal != '' && endVal != '')
			{			
				var testStartDate = new Date(startVal);
				var testEndDate = new Date(endVal);
				
				return testEndDate >= testStartDate;
			}
			
			return true;
		}
		
		//  ==================================================================
		//  Method         : createDatePicker
		//  Description    : create a date element
		//  @param         : component,inputId,object,label,value,defaultToday, canEdit
		//  ==================================================================
		function createDatePicker(component,inputId,object,label,value,defaultToday, canEdit)
		{
			if(value == null)
			{
				value = '';
			}
			else
			{
				value = new Date(value);
			}

			var accessLevel = getAccessLevel(object, inputId);
			
			if(accessLevel == 'Read')
			{
				return getReadOnlyField(component,inputId,label,value);
			}
			else if(accessLevel == 'None')
			{
				return null;
			}
			
			var datePickerDiv = $('<div class="voteControl votesDatepicker"/>');
			var datePickerLabel = $('<div class="voteControlLabel" "for="' + inputId + '">' + label + '</div>').appendTo(datePickerDiv);
			var datePickerSpan = $('<div class="votesDateControl"/>').appendTo(datePickerDiv);
			var datePicker = $('<input id="' + inputId + '"/>')
				.datepicker()
				.appendTo(datePickerSpan);
			if(value != null && value !== '')
			{
				datePicker.datepicker("setDate", ACE.DateUtil.getUTCDate(new Date(value).getTime()));
			}
			else if(defaultToday)
			{
				datePicker.datepicker("setDate", null);
			}		
			if(canEdit != true)
			{
				datePicker[0].disabled = true;	
			}
			else
			{
				$(datePicker).on('change', function() 
				{			
					var dVal = $(datePicker).val();
					if(inputId == 'Start_Date__c' || inputId == 'End_Date__c')
					{
						if(checkDateRange())
						{
							accountVotingPeriodToUpdate[inputId] = new Date(dVal) - new Date(dVal).getTimezoneOffset() * 60000;	
							$(saveButton).show(333);
							nonLOBSectionHasChanges = true; 
						}
						else
						{
							ACE.FaultHandlerDialog.show({title:'Invalid Date Range, please ensure the Voting End Date comes after the Voting Start Date',message : 'Please select a valid date range', error : 'Please review you data and try again'});
						}
					}
					else
					{
						accountVotingPeriodToUpdate[inputId] = new Date(dVal) - new Date(dVal).getTimezoneOffset() * 60000;
						$(saveButton).show(333);
						nonLOBSectionHasChanges = true; 
					}	
					
				});
			}		
			
			$(component).append(datePickerDiv); 
			
			return datePicker; 
		}
		
		function getStringDate(dVal)
		{
			var dateValue = null;
			if(dVal != null && dVal !== '')
			{
				/*var dateMilli = new Date(dVal).getTime();
				var minutesOffSet = new Date(dVal).getTimezoneOffset();
				var dateMilliToSet = dateMilli + (minutesOffSet * -60000);
				dateValue = new Date(dateMilliToSet).toISOString();
				dateValue = dateMilliToSet;*/

				dateValue = ACE.DateUtil.getUTCDate(new Date(dVal).getTime()) /*- (new Date(dVal).getTimezoneOffset() * 60000)*/;
			}	
			var stringDate;
			
			if(dateValue == null || dateValue === '' )
			{
				dateValue = null;
			}
			
			if(dateValue != null)
			{
				stringDate = dateValue;
			}
			else
			{
				stringDate = null;
			}	
			
			return 	dateValue.getTime();
		}
		
		//  ==================================================================
		//  Method         : createDiv
		//  Description    : create a div element
		//  @param         : component, width, align, divClassName
		//  ==================================================================
		function createDiv(component, width, align, divClassName)
		{
			var thisDiv = divClassName !== undefined ? $('<div class="' + divClassName +'"/>'):
													$('<div style="width:'+ width + ';padding-bottom:2px;display:inline-block;text-align:' + align + ';vertical-align:top" />');
			thisDiv.appendTo(component);
			return thisDiv;
		}
		

		//  ==================================================================
		//  Method         : getEvalField
		//  Description    : retrieve api value for evaluation type
		//  @param         : evalValue
		//  ==================================================================
		function getEvalField(evalValue)
		{
			if(evalValue == 'Rank')
			{
				return 'Rank__c';
			}	
			else if(evalValue == 'Comm')
			{
				return 'Percent_Commission__c';
			}
			else if(evalValue == 'Tier')
			{
				return 'Tier__c';
			}
			else if(evalValue == 'VoteShare')
			{
				return 'Vote_Share__c';
			}
			else if(evalValue == 'MktShare')
			{
				return 'Market_Share__c';
			}
		}

		//  ==================================================================
		//  Method         : getEvalFieldOutOf
		//  Description    : retrieve api value for the out of for evaluation type
		//  @param         : evalValue
		//  ==================================================================
		function getEvalFieldOutOf(evalValue)
		{
			if(evalValue == 'Rank')
			{
				return 'Rank_Out_Of__c';
			}	
			else if(evalValue == 'Comm')
			{
				return 'Percent_Commission_Out_Of__c';
			}
			else if(evalValue == 'Tier')
			{
				return 'Tier_Out_Of__c';
			}
			else if(evalValue == 'VoteShare')
			{
				return 'Vote_Share_Out_Of__c';
			}
			else if(evalValue == 'MktShare')
			{
				return 'Market_Share_Out_Of__c';
			}
		}

		//  ==================================================================
		//  Method         : createVoteSeriesPicklist
		//  Description    : create a vote series pick list
		//  @param         : compon,fieldName,label,defaultPicklist,dataprovider, fieldValue, canEdit
		//  ==================================================================
		Vote.prototype.createVoteSeriesPicklist = function (compon,fieldName,defaultPicklist,canEdit)
		{
			var fieldValue = this.dataprovider[fieldName];

			var picklistValues = [
				{label:"Select One",data:"",defaultSelected:false}
			];

			for(var x = 0;x < defaultPicklist.length; x++)
			{
				var picklistValue = defaultPicklist[x];
				var newVoteSeriesPickList =  {};
				newVoteSeriesPickList.label = picklistValue;
				newVoteSeriesPickList.data = picklistValue;
				if(picklistValue == fieldValue)
				{
					newVoteSeriesPickList.defaultSelected = true;                			
				}
				else
				{
					newVoteSeriesPickList.defaultSelected = false;
				}
				picklistValues.push(newVoteSeriesPickList);
			}

			var menuButton = new ACEMenuButton($(compon),{ dataProvider : picklistValues});
			
			if(canEdit != true)
			{
				menuButton.disabled = true;
			}
			else
			{
				menuButton.on('click', function() 
				{
					$(saveButton).show(333);
					nonLOBSectionHasChanges = true;
				});
			}
			
			menuButton.isPicklist = true;
			menuButton.id = fieldName;

			this.inputs.push(menuButton);
		}

		Vote.prototype.attachPicklistToVote = function(component,picklist,picklistValues,fieldName,disabled)
		{
			var tempPicklist = $(picklist);
			var fieldValue = this.dataprovider[fieldName];

			var accessLevel = getAccessLevel('Vote__c', fieldName);
			
			if(accessLevel == 'Read')
			{
				disabled = true;
			}
			else if(accessLevel == 'None')
			{
				return null;
			}

			if(fieldValue != null)
			{
				$(tempPicklist).empty();

				var tempValues = picklistValues;
				var setDefaultFromValue = false;

				for(var x=0;x<tempValues.length;x++)
				{
					var plv = tempValues[x];
					plv.defaultSelected = false;
					if(plv.data == fieldValue)
					{
						plv.defaultSelected = true;
						setDefaultFromValue = true;
					}
				}

				if(!setDefaultFromValue)
				{
					tempValues.push({label:fieldValue,data:fieldValue,defaultSelected:true});
				}

				for(var x=0;x<tempValues.length;x++)
				{
					pv = tempValues[x];
					var selected = '';
					if(pv.defaultSelected)
					{
						selected = 'selected';
					}
					if(setDefaultFromValue && pv.data != '')
					{
						$(tempPicklist).append('<option ' + selected + ' value="'+pv.data+'">' + pv.label + '</option>');
					}				
				}
			}

			$(tempPicklist).prop('disabled',disabled);
			tempPicklist.isRegularPicklist = true;

			$(tempPicklist).appendTo(component);
			tempPicklist.id = fieldName;
			this.inputs.push(tempPicklist);

			$(tempPicklist).change(function() 
			{
				$(saveButton).show(333);
				enhancedLOBHasChanges = true; 
			});

			return tempPicklist;
		}

		function createEnhancedVotePicklist(localPLVs)
		{
			var menuButton = '<select>';
			for(var x=0;x<localPLVs.length;x++)
			{
				pv = localPLVs[x];
				var selected = '';
				if(pv.defaultSelected)
				{
					selected = 'selected';
				}
				menuButton += '<option ' + selected + ' value="'+pv.data+'">' + pv.label + '</option>';
			}

			menuButton += '</select>';

			return menuButton;
		}

		//  ==================================================================
		//  Method         : createVotePicklist
		//  Description    : create a vote picklist 
		//  @param         : compon,fieldName,label,fieldValue,dataprovider,compareFieldName, canEdit
		//  ==================================================================
		Vote.prototype.createVotePicklist = function(compon,fieldName,label,compareFieldName,canEdit)
		{
			var fieldValue = this.dataprovider[fieldName];

			var picklistDiv = $('<div class="rowPickerWrapper" />').appendTo(compon);
			var labelDiv = $('<div>' + label + '</div>').appendTo(picklistDiv);		
					
			if(fieldValue == null)
			{
				fieldValue = '';
			}
								
			var picklistValues = [
				{label:"Select One",data:"",defaultSelected:false},
				{label:"Rank",data:"Rank",defaultSelected:false},
				{label:"% Comm",data:"Comm",defaultSelected:false},
				{label:"Tier",data:"Tier",defaultSelected:false},
				{label:"Vote Share",data:"VoteShare",defaultSelected:false},
				{label:"Market Share",data:"MktShare",defaultSelected:false}
			];
			
			for(var x = 0;x < picklistValues.length; x++)
			{
				var picklistValue = picklistValues[x];
				if(picklistValue.data == fieldValue)
				{
					picklistValue.defaultSelected = true;                			
				}
				else
				{
					picklistValue.defaultSelected = false;
				}
			}
			
			var menuButton = new ACEMenuButton($(picklistDiv),{ dataProvider : picklistValues});
			menuButton._container[0].attributes[0].value = 'aceMenuButtonAccountVote' + fieldName;
						
			var defaultValue = menuButton.val();
			this.dataprovider[fieldName] = defaultValue;
		
			var evalField = getEvalField(defaultValue);
			var evalFieldOutOf = getEvalFieldOutOf(defaultValue);
			
			$(compon).append(picklistDiv);            	
						
			var evalColumnField = $('<div class="rowInputWrapper" />').appendTo(compon);
			var evalColumnOutOf = $('<div class="rowInputWrapper" />').appendTo(compon);
			
			var evalFieldInput = this.createVoteInput(evalColumnField,evalField,true,acctVoteInfo.canEdit);
			var evalOutofInput = this.createVoteInput(evalColumnOutOf,evalFieldOutOf,true,acctVoteInfo.canEdit);

			var previousValue = menuButton.val();

			var accessLevel = getAccessLevel('Vote__c', fieldName);
			
			if(accessLevel == 'Read')
			{
				canEdit = false;
			}

			if(canEdit != true)
			{
				menuButton.disabled = true;
			}
			else
			{
				menuButton.on('click', function() 
				{
					
					var compareEval = $(menuButton._container[0].parentElement
																.parentElement
																.parentElement)
																.find('[data-type="' + 'aceMenuButtonAccountVote'  + compareFieldName + '"]');
															
					if(compareEval[0].children[0].innerText ===  menuButton.text() && compareEval[0].children[0].innerText !== 'Select One')
					{
						ACEConfirm.show
						(
							'Cannot have the same Eval Type',
							"Save Error",
							[ "OK::confirmButton"],
							function(result)
							{								
								menuButton.setSelectedItem(previousValue);							
							}
						);	
					}
					else
					{			
						var fieldVal = menuButton.val();

						evalField = getEvalField(fieldVal);
						evalFieldOutOf = getEvalFieldOutOf(fieldVal);
						
						$(evalFieldInput)[0].id = evalField;
						$(evalOutofInput)[0].id = evalFieldOutOf;
						
						previousValue = fieldVal;
						//trigger has changes
						
					}

					$(saveButton).show(333);
					nonLOBSectionHasChanges = true;
				});
			}
			menuButton.isPicklist = true;
			this.inputs.push(menuButton);

			menuButton.id = fieldName;

			return menuButton;
		}

		//  ==================================================================
		//  Method         : createLOBTabs
		//  Description    : create tabs for each lob
		//  @param         : component,lobs
		//  ==================================================================
		function createLOBTabs(component,lobs)
		{
			if(lobs != null)
			{
				var setTabLabels = false;// lobs.length < 10;
				var picklistValues = [];
				var picklistDiv;
				
				if(!setTabLabels)
				{
					picklistDiv = $('<div id="picklistDiv"/>').appendTo(component);
				}	
				
				var tabs = $('<div id="lobTabs" class="latabs" selected="0"/>').appendTo(component);
				var subTabs = $('<div class="latabs" selected="0"/>');
				
				for(var x = 0; x < lobs.length; x++)
				{
					var tabLabel = '';
					var tabValue = '';
					var thisLOB = lobs[x];

					if(setTabLabels)
					{
						tabLabel = thisLOB.LOB;
						tabValue = thisLOB.LOB;
					}
					else
					{
						/*
						if(thisLOB.isEnhancedLOB)
						{
							if(lobs[x].LOB == "Global Equities")
							{
								tabValue = 'GlobalEQ';	
								picklistValues.push({label : lobs[x].LOB, data : tabValue, defaultSelected : (x==0)});
							}
							else if(lobs[x].LOB == "Global Equities Research")
							{
								// do nothing
							}
						}
						else
						{*/
							tabValue = lobs[x].LOBId;
							picklistValues.push({label : lobs[x].LOB, data : lobs[x].LOBId, defaultSelected : (lobs[x].LOBId == defaultLOB)});
						//}
					}							
					
					var thisLOB = lobs[x];							
					
					var lobNotes = '';
					if(thisLOB.vote !== undefined && thisLOB.vote !== null)
					{
						lobNotes = thisLOB.vote.Notes__c;
					}	
						
					var thisTab;
					
					/*if(thisLOB.LOB == "Global Equities")
					{					
						var thisParentTab = $('<LA-TAB id="GlobalEQ" class="mainTab" tag="Global Equities" />').appendTo(tabs);
						$(subTabs).appendTo(thisParentTab);
						thisTab = $('<LA-TAB class="subTab enhanced" id="' + tabValue + '" tag="Global Equities"/>').appendTo(subTabs);
					}
					else if(thisLOB.LOB == "Global Equities Research")
					{
						thisTab = $('<LA-TAB class="subTab" id="' + tabValue + '" tag="Global Equities Research" />').appendTo(subTabs);
					}*/

					if(thisLOB.isEnhancedLOB)
					{
						thisTab = $('<div class="mainTab lobTab enhanced" id="' + tabValue + '" tag="' + tabLabel + '"/>').appendTo(tabs);
					}
					else
					{
						thisTab = $('<div class="mainTab lobTab regular" id="' + tabValue + '" tag="' + tabLabel + '" />').appendTo(tabs);
					}
	/*
					if(x==0)
					{
						if(thisLOB.isEnhancedLOB)
						{
							createEnhancedVoteContainer(thisTab,thisLOB);
						}		
						else
						{
							createRegularVoteContainer(thisTab,thisLOB);
						}

						$(thisTab).addClass('hasInit');
					}
	*/
				}
				
				if(!setTabLabels)
				{
					var tabMenu = $(tabs).find('#tabs-menu').empty();
					var menuButton = new ACEMenuButton($(picklistDiv),{ dataProvider : picklistValues});
					tabMenu.append(picklistDiv);
					var tabBodies = $('#lobTabs');
					
					var pickVal = '#' + menuButton.val();

					$(tabBodies).find('.lobTab').hide();//removeClass('la-selected');
					$(tabBodies).find(pickVal).show();//('la-selected');

					var previousValue = menuButton.val(); // cache the previous value so we can switch back to it if switching to LOB is cancelled
						
					// menuButton.on('click', function()
					menuButton.on('click', function(e)
					{
						if(acctVoteInfo.purgeOnTabChange && enhancedLOBHasChanges)
						{
							ACEConfirm.show
							(
								acctVoteInfo.purgeWarningMessage,
								"Please Confirm",
								[ "Confirm::confirmButton", "Cancel::cancelButton"],
								function(result)
								{
									if (result === "Confirm")
									{
										onLOBChange(menuButton,tabBodies,lobs);
										previousValue = e.item.data;
									}
									else
									{
										menuButton.setSelectedItem(previousValue);		
									}
								}
							);
						}
						else
						{
							previousValue = e.item.data;
							onLOBChange(menuButton,tabBodies,lobs);
						}
					});

					var pickVal = '#' + menuButton.val();
						
					$(tabBodies).find('.lobTab').hide();
					$(tabBodies).find(pickVal).show();

					var newTab = $(tabBodies).find(pickVal);
					var isEnhanced = $(newTab).hasClass('enhanced');

					for(var i=0;i<lobs.length;i++)
					{
						var thisLOB = lobs[i];
						if(thisLOB.LOBId == $(newTab).attr('id'))
						{
							$(newTab).addClass('hasInit');

							if(isEnhanced)
							{
								createEnhancedVoteContainer(newTab,thisLOB);
							}
							else
							{
								createRegularVoteContainer(newTab,thisLOB);
								break;
							}	
							break;
						}									
					}
				}
				else
				{	
					tabs[0].selected = 0;
				}
			}	
		}

		function onLOBChange(menuButton,tabBodies,lobs)
		{
			enhancedLOBHasChanges = false; 
			if (!nonLOBSectionHasChanges && acctVoteInfo.purgeOnTabChange)
			{
				$(saveButton).hide();
			}

			$(loadingModal).show();
			var pickVal = '#' + menuButton.val();
			
			$(tabBodies).find('.lobTab').hide();
			$(tabBodies).find(pickVal).show();

			//$(tabBodies).find('.mainTab').removeClass('la-selected');
			var newTab = $(tabBodies).find(pickVal);
			var isEnhanced = $(newTab).hasClass('enhanced');
			//$(newTab).addClass('la-selected');

			var hasInit = $(newTab).hasClass('hasInit');

			if(hasInit && acctVoteInfo.purgeOnTabChange)
			{
				$(newTab).empty();	
				for(var i=0;i<lobs.length;i++)
				{
					var thisLOB = lobs[i];
					if(thisLOB.LOBId == $(newTab).attr('id'))
					{
						$(newTab).addClass('hasInit');
						
						if(isEnhanced)
						{
							createEnhancedVoteContainer(newTab,thisLOB);
						}
						else
						{
							createRegularVoteContainer(newTab,thisLOB);
							break;
						}	
						break;
					}									
				}		
			}
			else if (!hasInit)
			{
				for(var i=0;i<lobs.length;i++)
				{
					var thisLOB = lobs[i];
					if(thisLOB.LOBId == $(newTab).attr('id'))
					{
						$(newTab).addClass('hasInit');
						
						if(isEnhanced)
						{
							createEnhancedVoteContainer(newTab,thisLOB);
						}
						else
						{
							createRegularVoteContainer(newTab,thisLOB);
							break;
						}	
						break;
					}									
				}
			}

			

			$(loadingModal).hide();
		}

		function createRegularVoteContainer(thisTab, thisLOB)
		{
			var approvalTabPageRow = $('<div class="approvalTabPageRow" />').appendTo(thisTab);
			var tabRowSection = $('<div class="tabRowSection" />').appendTo(approvalTabPageRow);
			var tabSectionHeaderLabel = $('<div class="tabSectionHeader"><div class="tabSectionHeaderLabel"></div></div>').appendTo(tabRowSection);
			var tabSectionInnerWrapper = $('<div class="tabSectionInnerWrapper" />').appendTo(tabRowSection);
			var tabSectionGrid = $('<div class="tabSectionGrid" />').appendTo(tabSectionInnerWrapper);

			//create rank and tier divs
			var tabSectionHeaderRow = $('<div class="tabSectionHeaderRow" />').appendTo(tabSectionGrid);
			$('<div class="headerColumn spacerheaderColumn" />').appendTo(tabSectionHeaderRow);
			$('<div class="headerColumn"><div class="columnHeaderRow"><div class="columnHeaderLabel">Rank</div></div></div>').appendTo(tabSectionHeaderRow);
			$('<div class="headerColumn"><div class="columnHeaderRow"><div class="columnHeaderLabel">Tier</div></div></div>').appendTo(tabSectionHeaderRow);

			var gridBodyContainer = $('<div class="gridBodyContainer" />').appendTo(tabSectionGrid);
			
			for(var y = 0; y < thisLOB.regionVotes.length; y++)
			{
				var tabSectionGridRow = $('<div class="tabSectionGridRow" />').appendTo(gridBodyContainer);
				var thisVote = new Vote(tabSectionGridRow,thisLOB.regionVotes[y],false,null,false);
				//votes.push(thisVote.dataprovider);
				
				if(y==0)
				{
					var tabRowSectionText = $('<div class="tabRowSection tabRowSectionText" />').appendTo(approvalTabPageRow);
					$('<div class="tabSectionHeader"><div class="tabSectionHeaderLabel">' + thisLOB.LOB + ' Notes' + '</div></div>').appendTo(tabRowSectionText);
					var notesContainer = $('<div class="tabSectionInnerWrapper" />').appendTo(tabRowSectionText);

					thisVote.createNotesInput(notesContainer,'Notes__c',thisLOB.LOB + ' Notes','textarea', acctVoteInfo.canEdit);
				}

				voteWrappers.push(thisVote);
			}	
			
			var empContainer = $('<div class="approvalTabPageRow" />').appendTo(thisTab);
			
			for(var y = 0; y < thisLOB.regionVotes.length; y++)
			{
				var empVotes = thisLOB.regionVotes[y].employeeVotes;

				if(empVotes != null)
				{	
					var regContainer = $('<div class="tabRowSection" />').appendTo(empContainer);
					$('<div class="tabSectionHeader"><div class="tabSectionHeaderLabel">' + thisLOB.regionVotes[y].region + '</div></div>').appendTo(regContainer);	
					
					tabSectionInnerWrapper = $('<div class="tabSectionInnerWrapper" />').appendTo(regContainer);
					tabSectionGrid = $('<div class="tabSectionGrid" />').appendTo(tabSectionInnerWrapper);
		
					//create rank and tier divs
					tabSectionHeaderRow = $('<div class="tabSectionHeaderRow" />').appendTo(tabSectionGrid);
					$('<div class="headerColumn spacerheaderColumn" />').appendTo(tabSectionHeaderRow);
					$('<div class="headerColumn"><div class="columnHeaderRow"><div class="columnHeaderLabel">Rank</div></div></div>').appendTo(tabSectionHeaderRow);
					$('<div class="headerColumn"><div class="columnHeaderRow"><div class="columnHeaderLabel">Tier</div></div></div>').appendTo(tabSectionHeaderRow);
					$('<div class="headerColumn"><div class="columnHeaderRow"><div class="columnHeaderLabel">Points</div></div></div>').appendTo(tabSectionHeaderRow);
					gridBodyContainer = $('<div class="gridBodyContainer" />').appendTo(tabSectionGrid);
						
					if(!Array.isArray(empVotes))
					{
						empVotes = [empVotes];
					}	

					for(var i = 0; i < empVotes.length;i++)
					{
						var tabSectionGridRow = $('<div class="tabSectionGridRow" />').appendTo(gridBodyContainer);
						var thisEmpVote = new Vote(tabSectionGridRow,empVotes[i],false,null,false);
						//votes.push(thisEmpVote.dataprovider);
						voteWrappers.push(thisEmpVote);
					}
				}
			}			
		}

		function showHideTeams()
		{
			if(teamSelected)
			{
				$('.teamContainer').show();
				$('.employeeContainer').hide();
			}
			else
			{
				$('.teamContainer').hide();
				$('.employeeContainer').show();
			}
		}

		function createEnhancedVoteContainer(thisTab, thisLOB)
		{
			if(acctVoteInfo.purgeOnTabChange)
			{
				tempVoteWrappers = [];
			}		

			var approvalTabPageRow = $('<div class="flex-parent votes-container" />').appendTo(thisTab);

			var leftThird = $('<div class="is-third votes-left-column" />').appendTo(approvalTabPageRow);
			var tabRowSection = $('<div class="flex-parent is-vertical" />').appendTo(leftThird);
			//var tabSectionHeaderLabel = $('<p class="votes-label">' + thisLOB.LOB + '</p>').appendTo(tabRowSection);
			var tabSectionTable = $('<table class="vote-table"/>').appendTo(tabRowSection);

			var tabSectionHeaderRow = $('<tr class="vote-table-row vote-header-row">').appendTo(tabSectionTable);
			$('<td class="vote-table-cell vote-header-column">Region</td>').appendTo(tabSectionHeaderRow);
			$('<td class="vote-table-cell vote-header-column">Rank</td>').appendTo(tabSectionHeaderRow);
			$('<td class="vote-table-cell vote-header-column">Tier</td>').appendTo(tabSectionHeaderRow);		

			for(var y = 0; y < thisLOB.regionVotes.length; y++)
			{
				var tabSectionGridRow = $('<tr class="vote-table-row vote-data-row"/>').appendTo(tabSectionTable);
				var thisVote = new Vote(tabSectionGridRow,thisLOB.regionVotes[y],false,null,true);

				if(y==0)
				{
					var notesLabel = $('<p class="votes-label">' + thisLOB.LOB + '</p>').appendTo(tabRowSection);
					var notesParent = $('<div class="votes-textarea">').appendTo(tabRowSection);				

					thisVote.createNotesInput(notesParent,'Notes__c',thisLOB.LOB + ' Notes','textarea', acctVoteInfo.canEdit);
				}

				tempVoteWrappers.push(thisVote);
			}

			// Right Pane

			var rightPane = $('<div class="el-block votes-right-column"/>').appendTo(approvalTabPageRow);

			// Row flexbox column
			var rightFlexParent = $('<div class="flex-parent is-vertical"/>').appendTo(rightPane);
			$('<p class="votes-info-text">Use the drop-down menu to select a region that you would like to see.</p>').appendTo(rightFlexParent);

			// Row flexbox
			var rowFlexBox = $('<div class="el-row flex-parent region-wrapper"/>').appendTo(rightFlexParent);
			var regionPickerContainer = $('<div class="region-container flex-parent"/>').appendTo(rowFlexBox);
			
			$('<label>Region</label>').appendTo(regionPickerContainer);
			var regionPicklistDiv = $('<div/>').appendTo(regionPickerContainer);
			var regions = thisLOB.regionVotes;
			var regionPicklistValues = [{data:"All", defaultSelected:true, label: 'All'}];
			for(var i=0;i<regions.length;i++)
			{
				var thisRegion = regions[i];
				regionPicklistValues.push({data: thisRegion.regionId, defaultSelected:false, label: thisRegion.region});
			}

			var regionMenuButton = new ACEMenuButton($(regionPicklistDiv),{ dataProvider : regionPicklistValues});
			
			regionMenuButton.on('click',function()
			{
				var regionVal = regionMenuButton.val();
				if(regionVal == 'All')
				{
					$(thisTab).find('.regionContainer').show();
					showHideTeams();
				}
				else
				{
					$(thisTab).find('.regionContainer').hide();
					$(thisTab).find('#' + regionVal).show();
					showHideTeams();
				}
			});

			// Button container
			var teamEmpButtonContainer = $('<div class="votes-button-container flex-parent"/>').appendTo(rowFlexBox);
			var teamButton = $('<button class="votes-btn-teams selected">Teams</button>').appendTo(teamEmpButtonContainer);
			var empButton = $('<button class="votes-btn-individuals">Individuals</button>').appendTo(teamEmpButtonContainer);
			
			$(teamButton).click(function()
			{
				teamSelected = true;
				$(teamButton).addClass('selected');
				$(empButton).removeClass('selected');
				
				showHideTeams();
			});

			$(empButton).click(function()
			{
				teamSelected = false;
				$(empButton).addClass('selected');
				$(teamButton).removeClass('selected');
				
				showHideTeams();		
			});

			// case where its a normal enhanced view. 
			if (!acctVoteInfo.lobsToSeparateByTeam.includes(thisLOB.LOB))
			{
				var employeesToPutInTable = new Set();

				for (var prop in acctVoteInfo.regionEmpMap) {
					if (Object.prototype.hasOwnProperty.call(acctVoteInfo.regionEmpMap, prop)) 
					{
						if (prop.includes(thisLOB.LOBId))
						{
							employeesToPutInTable.add(acctVoteInfo.regionEmpMap[prop]);
						}
					}
				}

				for(var y = 0; y < thisLOB.regionVotes.length; y++)
				{
					var teamVotes = thisLOB.regionVotes[y].teamVotes;

					var employeeVotes = thisLOB.regionVotes[y].employeeVotes;

					// looping through teams
					if(teamVotes != null)
					{	
						if (teamVotes.length > 0)
						{
							var regionContainer = $('<div id="' + thisLOB.regionVotes[y].regionId + '" class="regionContainer"/>').appendTo(rightPane);
						
							var hasCreatedTeamTabRegionTable = false; 
							var teamTable;

							for (var teamIdx = 0; teamIdx < teamVotes.length; teamIdx++)
							{
								var teamVote = teamVotes[teamIdx];

								if(teamVote.voteInfos != null && !Array.isArray(teamVote.voteInfos))
								{
									teamVote.voteInfos = [teamVote.voteInfos];
								}

								for(var i = 0; i < teamVote.voteInfos.length;i++)
								{
									var thisVote = teamVote.voteInfos[i];
									var thisVoteRow;
				
									var thisEmpVote;

									if(thisVote.isTeamVote)
									{
										if (!hasCreatedTeamTabRegionTable)
										{
											hasCreatedTeamTabRegionTable = true; 

											var teamContainer = $('<div class="flex-parent is-vertical teamContainer"><p class="votes-label" style="font-weight:bold;">' + thisLOB.regionVotes[y].region + '</p></div>').appendTo(regionContainer);
											// var applyNoVoteCheckboxContainer = $('<label class="control checkbox"></label>').appendTo(teamContainer);
											var applyNoVoteCheckboxContainer = $('<div class="apply-no-vote-flex-parent"></div>').appendTo(teamContainer);
											var applyNoVoteCheckboxLabel = $('<label class="control checkbox votes-label">Apply No Vote</label>').appendTo(applyNoVoteCheckboxContainer);
											var applyNoVoteCheckbox = $('<input type="checkbox" class="apply-no-vote-checkbox"></input>').appendTo(applyNoVoteCheckboxContainer);
											teamTable = $('<table class="vote-table"/>').appendTo(teamContainer);
							
											var teamHeaderRow = $('<tr class="vote-table-row vote-header-row"/>').appendTo(teamTable);
											$('<td class="vote-table-cell vote-header-column">Team</td>').appendTo(teamHeaderRow);
											$('<td class="vote-table-cell vote-header-column">Rank</td>').appendTo(teamHeaderRow);
											$('<td class="vote-table-cell vote-header-column">Target Rank</td>').appendTo(teamHeaderRow);
											$('<td class="vote-table-cell vote-header-column">Points</td>').appendTo(teamHeaderRow);
											$('<td class="vote-table-cell vote-header-column">Target Points</td>').appendTo(teamHeaderRow);
											$('<td class="vote-table-cell vote-header-column votes-note-column">Notes</td>').appendTo(teamHeaderRow);
											$('<td class="vote-table-cell vote-header-column votes-account-contact-column">Account Contact(s)</td>').appendTo(teamHeaderRow);
										}
										thisVoteTeamRow = $('<tr class="vote-table-row vote-data-row"/>').appendTo(teamTable);
										thisEmpVote = new Vote(thisVoteTeamRow,teamVote.voteInfos[i],false,null,true, applyNoVoteCheckbox);
									}

									tempVoteWrappers.push(thisEmpVote);
								}
							}	
							
						}

					}

					// looping through emps. 
					if (employeeVotes != null)
					{
						if (employeeVotes.length > 0)
						{
							var hasCreateEmployeeTableForRegion = false; 
							var regionContainer = $('<div id="' + thisLOB.regionVotes[y].regionId + '" class="regionContainer"/>').appendTo(rightPane);
							var regionLabel = $('<div class="flex-parent is-vertical employeeContainer"><p class="votes-label" style="font-weight:bold;">' + thisLOB.regionVotes[y].region + '</p></div>').appendTo(regionContainer);
							var applyNoVoteCheckboxContainer = $('<div class="apply-no-vote-flex-parent"></div>').appendTo(regionLabel);
							var applyNoVoteCheckboxLabel = $('<label class="control checkbox votes-label">Apply No Vote</label>').appendTo(applyNoVoteCheckboxContainer);
							var applyNoVoteCheckboxForEmps = $('<input type="checkbox" class="apply-no-vote-checkbox"></input>').appendTo(applyNoVoteCheckboxContainer);

							for (var i = 0; i < employeeVotes.length; i++)
							{

								if (!hasCreateEmployeeTableForRegion)
								{
									// Table Containers
									
									var empContainer = $('<div id="' + thisLOB.regionVotes[y].regionId + '" class="flex-parent is-vertical employeeContainer"><p class="votes-label"></p></div>').appendTo(regionContainer);
									var empTable = $('<table class="vote-table"/>').appendTo(empContainer);
									
									empHeaderRow = $('<tr class="vote-table-row vote-header-row"/>').appendTo(empTable);
									
									$('<td class="vote-table-cell vote-header-column">Employee</td>').appendTo(empHeaderRow);
									$('<td class="vote-table-cell vote-header-column">Rank</td>').appendTo(empHeaderRow);
									$('<td class="vote-table-cell vote-header-column">Target Rank</td>').appendTo(empHeaderRow);
									$('<td class="vote-table-cell vote-header-column">Points</td>').appendTo(empHeaderRow);
									$('<td class="vote-table-cell vote-header-column">Target Points</td>').appendTo(empHeaderRow);
									$('<td class="vote-table-cell vote-header-column votes-note-column">Notes</td>').appendTo(empHeaderRow);
									$('<td class="vote-table-cell vote-header-column votes-account-contact-column">Account Contact(s)</td>').appendTo(empHeaderRow);
									hasCreateEmployeeTableForRegion= true; 
								}
								thisVoteRow = $('<tr class="vote-table-row vote-data-row"/>').appendTo(empTable);
								var thisEmpVote = new Vote(thisVoteRow,employeeVotes[i],false,null,true,applyNoVoteCheckboxForEmps);
								tempVoteWrappers.push(thisEmpVote);
							}
							
						}
					}


				}
			}
			else
			{
				for(var y = 0; y < thisLOB.regionVotes.length; y++)
				{
					var teamVotes = thisLOB.regionVotes[y].teamVotes;

					if(teamVotes != null)
					{	
						if (teamVotes.length <= 0)
						{
							continue;
						}

						var regionContainer = $('<div id="' + thisLOB.regionVotes[y].regionId + '" class="regionContainer"/>').appendTo(rightPane);
						
						var hasCreatedTeamTabRegionTable = false; 
						var teamTable;

						// var regionLabel = $('<div class="flex-parent is-vertical employeeContainer"><p class="votes-label" style="font-weight:bold;">' + thisLOB.regionVotes[y].region + '<p class="votes-label">').appendTo(regionContainer);
						var regionLabel = $('<div class="flex-parent is-vertical employeeContainer"><p class="votes-label" style="font-weight:bold;">' + thisLOB.regionVotes[y].region + '</p></div>').appendTo(regionContainer);
						var applyNoVoteCheckboxContainer = $('<div class="apply-no-vote-flex-parent"></div>').appendTo(regionLabel);
						var applyNoVoteCheckboxLabel = $('<label class="control checkbox votes-label">Apply No Vote</label>').appendTo(applyNoVoteCheckboxContainer);
						var applyNoVoteCheckboxForEmps = $('<input type="checkbox" class="apply-no-vote-checkbox"></input>').appendTo(applyNoVoteCheckboxContainer);
						
						for (var teamIdx = 0; teamIdx < teamVotes.length; teamIdx++)
						{

							// Table Containers
							var empContainer = $('<div id="' + thisLOB.regionVotes[y].regionId + '" class="flex-parent is-vertical employeeContainer"><p class="votes-label">' + teamVotes[teamIdx].teamName + '</p></div>').appendTo(regionContainer);
							var empTable = $('<table class="vote-table"/>').appendTo(empContainer);
							
							empHeaderRow = $('<tr class="vote-table-row vote-header-row"/>').appendTo(empTable);
							
							$('<td class="vote-table-cell vote-header-column">Employee</td>').appendTo(empHeaderRow);
							$('<td class="vote-table-cell vote-header-column">Rank</td>').appendTo(empHeaderRow);
							$('<td class="vote-table-cell vote-header-column">Target Rank</td>').appendTo(empHeaderRow);
							$('<td class="vote-table-cell vote-header-column">Points</td>').appendTo(empHeaderRow);
							$('<td class="vote-table-cell vote-header-column">Target Points</td>').appendTo(empHeaderRow);
							$('<td class="vote-table-cell vote-header-column votes-note-column">Notes</td>').appendTo(empHeaderRow);
							$('<td class="vote-table-cell vote-header-column votes-account-contact-column">Account Contact(s)</td>').appendTo(empHeaderRow);

							var teamVote = teamVotes[teamIdx];

							if(teamVote.voteInfos != null && !Array.isArray(teamVote.voteInfos))
							{
								teamVote.voteInfos = [teamVote.voteInfos];
							}

							for(var i = 0; i < teamVote.voteInfos.length;i++)
							{
								var thisVote = teamVote.voteInfos[i];
								var thisVoteRow;
			
								var thisEmpVote;

								if(thisVote.isTeamVote)
								{
									// thisVoteRow = $('<tr class="vote-table-row vote-data-row"/>').appendTo(teamTable);
									
									if (!hasCreatedTeamTabRegionTable)
									{
										hasCreatedTeamTabRegionTable = true; 

										var teamContainer = $('<div class="flex-parent is-vertical teamContainer"><p class="votes-label" style="font-weight:bold;">' + thisLOB.regionVotes[y].region + '</p></div>').appendTo(regionContainer);
										// var applyNoVoteCheckboxContainer = $('<label class="control checkbox"></label>').appendTo(teamContainer);
										var applyNoVoteCheckboxContainer = $('<div class="apply-no-vote-flex-parent"></div>').appendTo(teamContainer);
										var applyNoVoteCheckboxLabel = $('<label class="control checkbox votes-label">Apply No Vote</label>').appendTo(applyNoVoteCheckboxContainer);
										var applyNoVoteCheckbox = $('<input type="checkbox" class="apply-no-vote-checkbox"></input>').appendTo(applyNoVoteCheckboxContainer);
										teamTable = $('<table class="vote-table"/>').appendTo(teamContainer);
						
										var teamHeaderRow = $('<tr class="vote-table-row vote-header-row"/>').appendTo(teamTable);
										$('<td class="vote-table-cell vote-header-column">Team</td>').appendTo(teamHeaderRow);
										$('<td class="vote-table-cell vote-header-column">Rank</td>').appendTo(teamHeaderRow);
										$('<td class="vote-table-cell vote-header-column">Target Rank</td>').appendTo(teamHeaderRow);
										$('<td class="vote-table-cell vote-header-column">Points</td>').appendTo(teamHeaderRow);
										$('<td class="vote-table-cell vote-header-column">Target Points</td>').appendTo(teamHeaderRow);
										$('<td class="vote-table-cell vote-header-column votes-note-column">Notes</td>').appendTo(teamHeaderRow);
										$('<td class="vote-table-cell vote-header-column votes-account-contact-column">Account Contact(s)</td>').appendTo(teamHeaderRow);
									}
									thisVoteTeamRow = $('<tr class="vote-table-row vote-data-row"/>').appendTo(teamTable);
									thisEmpVote = new Vote(thisVoteTeamRow,teamVote.voteInfos[i],false,null,true, applyNoVoteCheckbox);
								}
								else
								{
									thisVoteRow = $('<tr class="vote-table-row vote-data-row"/>').appendTo(empTable);
									thisEmpVote = new Vote(thisVoteRow,teamVote.voteInfos[i],false,null,true,applyNoVoteCheckboxForEmps);
								}

								tempVoteWrappers.push(thisEmpVote);
							}
						}				
					}
				}
			}
			

			$('.teamContainer').show();
			$('.employeeContainer').hide();
		}


		//  ==================================================================
		//  Method         : createVoteContainer
		//  Description    : create a div container
		//  @param         : component,thisRegionVote
		//  ==================================================================	
		function createVoteContainer(component,thisRegionVote)
		{
			//header info
			var votesSectionHeader = createDiv(component, '', '', 'votesSectionHeader');
			$(votesSectionHeader).append($('<div class="votesSectionHeaderLabel">Account Vote</div>'));

			//inner section
			var votesSectionInnerWrapper = createDiv(component, '', '', 'votesSectionInnerWrapper');
			var accountVoteWrapper       = createDiv(votesSectionInnerWrapper, '', '', 'accountVoteWrapper');		
			var accountVoteHeaderRow       = createDiv(accountVoteWrapper, '', '', 'accountVoteHeaderRow');
			$('<div class="headerColumn spacerheaderColumn" />').appendTo(accountVoteHeaderRow);
			var headerColumnOne   = $('<div class="headerColumn" />').appendTo(accountVoteHeaderRow); 
			var headerColumnTwo   = $('<div class="headerColumn" />').appendTo(accountVoteHeaderRow); 
			var headerColumnThree = $('<div class="headerColumn" />').appendTo(accountVoteHeaderRow); 
			
			//headerColumnOne
			$('<div class="columnHeaderRow" />').appendTo(headerColumnOne);
			var columnHeaderRow = $('<div class="columnHeaderRow" />').appendTo(headerColumnOne);
			$('<div class="columnHeaderLabel">Vote Series</div>').appendTo(columnHeaderRow);
			$('<div class="columnHeaderLabel">Label 1</div>').appendTo(columnHeaderRow);
			
			//headerColumnTwo
			$('<div class="columnHeaderRow"><div class="columnHeaderLabel">Eval 1</div></div>').appendTo(headerColumnTwo);
			columnHeaderRow = $('<div class="columnHeaderRow" />').appendTo(headerColumnTwo);
			$('<div class="columnHeaderLabel">Eval Type</div>').appendTo(columnHeaderRow);
			$('<div class="columnHeaderLabel">Points</div>').appendTo(columnHeaderRow);
			$('<div class="columnHeaderLabel">Out Of</div>').appendTo(columnHeaderRow);

			//headerColumnThree
			$('<div class="columnHeaderRow"><div class="columnHeaderLabel">Eval 2</div></div>').appendTo(headerColumnThree);
			columnHeaderRow = $('<div class="columnHeaderRow" />').appendTo(headerColumnThree);
			$('<div class="columnHeaderLabel">Eval Type</div>').appendTo(columnHeaderRow);
			$('<div class="columnHeaderLabel">Points</div>').appendTo(columnHeaderRow);
			$('<div class="columnHeaderLabel">Out Of</div>').appendTo(columnHeaderRow);

			var gridBodyContainer = $('<div class="gridBodyContainer" />').appendTo(accountVoteWrapper);		
			var regionVotes = thisRegionVote.regionVotes;
			if(regionVotes != null)
			{	
				for(var x = 0; x < regionVotes.length;x++)
				{	
					var thisRegVote = regionVotes[x];
					var thisVote = new Vote(gridBodyContainer,thisRegVote,true, {voteSeriesPicklist: thisRegionVote.voteSeriesPicklist});
					voteWrappers.push(thisVote);
				}
			}
		}
		
		//  ==================================================================
		//  Method         : getAccessLevel
		//  Description    : get accesslevel for each objectname and field
		//  @param         : objectName, fieldName
		//  ==================================================================
		function getAccessLevel(objectName, fieldName)
		{
			if(restrictions != null)
			{	
				multiRestrictions = [];
				for(var x = 0; x < restrictions.length; x++)
				{
					var restrict = restrictions[x];
					if(restrict.fieldName == fieldName && restrict.objectName == objectName)
					{
						multiRestrictions.push(restrict);					
					}
				}
				
				var currentAccess = 'Edit';
				
				if(multiRestrictions.length == 1)
				{
					return multiRestrictions[0].accessLevel;
				}
				
				for(var x = 0; x < multiRestrictions.length; x++)
				{
					var restrict = multiRestrictions[x];
					if(restrict == 'None')
					{
						return 'None';
					}
					else
					{
						currentAccess = restrict.accessLevel;
					}
				}
			
				return currentAccess;
			}
			
			return 'Edit';
		}
		

		//  ==================================================================
		//  Method         : createPicklist
		//  Description    : create a picklist element
		//  @param         : component,fieldName,objectName,label,fieldValue,divClassName, canEdit
		//  ==================================================================
		function createPicklist(component,fieldName,objectName,label,fieldValue,divClassName, canEdit)
		{	
			var picklistDiv = divClassName !== undefined? $('<div class="' + divClassName + '" />'): $('<div class="la-input" style="width:100%;font-size:14pt"/>');
			
			var labelDiv = $('<div class="voteControlLabel">' + label + '</div>').appendTo(picklistDiv);		
					
			if(fieldValue == null)
			{
				fieldValue = '';
			}

			var accessLevel = getAccessLevel(objectName, fieldName);
			if(accessLevel == 'Read')
			{
				return getReadOnlyField(component,fieldName,label,fieldValue);
			}
			else if(accessLevel == 'None')
			{
				return null;
			}
			
			var menuButton;
			
			ACE.Remoting.call
			(
				ACE.Namespace.CORE_APEX + "ACECoreController.getPicklistValues",
				[ objectName, fieldName ],
				function(result, event)
				{
					if (event.status)
					{
						var picklistValues = [];
						if(newNote)
						{
							var defaultValue = {data:"Select One", defaultSelected:true, label: 'Select One'};
		
							picklistValues.push(defaultValue);
						}

						for(var x = 0;x < result.length; x++)
						{
							var picklistValue = result[x];
							if(picklistValue.data == fieldValue)
							{
								picklistValue.defaultSelected = true;                			
							}
							else if (newNote && picklistValue.defaultSelected === true)
							{
								// ST-4997: If we already have a default set in SF, use that instead of 'Select One'
								defaultValue.defaultSelected = false;
							}
							else
							{
								picklistValue.defaultSelected = false;
							}
							
							if(picklistValue != null)
							{
								picklistValues.push(picklistValue);
							}	
						}
						
						menuButton = new ACEMenuButton($(picklistDiv),{ dataProvider : picklistValues});
						
						picklistDiv[0].menuButton = menuButton;
						
						if(accessLevel == 'Read')
						{
							menuButton.prop('disabled',true);
						}

						defaultValue = menuButton.val();
						accountVotingPeriodToUpdate[fieldName] = defaultValue;
						
						if(fieldName == 'Period__c')
						{
							periodDataProvider = picklistValues;
						}
						
						if(canEdit != true)
						{
							menuButton.disabled = true;
						}
						else
						{
							menuButton.on('click', function() 
							{
								fieldVal = menuButton.val();
								if(fieldVal === '')
								{
									fieldVal = null;
								}
								
								accountVotingPeriodToUpdate[fieldName] = fieldVal;
								
								if(fieldName == 'Period__c')
								{							
									validatDateFields(false, fieldVal);
								}
								
								$(saveButton).show(333);
								nonLOBSectionHasChanges = true; 
							});						
						}
						
						validatDateFields(false, defaultValue, null, true);
					}
				}
			);
			
			$(component).append(picklistDiv);
			return picklistDiv;
		}

		//  ==================================================================
		//  Method         : createMultiSelectField
		//  Description    : create a multi picklist field
		//  @param         : component, fieldName,objectName,label,value, canEdit
		//  ==================================================================
		function createMultiSelectField(component, fieldName,objectName,label,value, canEdit)
		{
			var picklistDiv = $('<div  class="voteControl votesTypePicker" />');
			var labelDiv = $('<div class="voteControlLabel">' + label + '</div>').appendTo(picklistDiv);		
					
			if(value == null)
			{
				value = 'Click here to add values';
			}

			var accessLevel = getAccessLevel(objectName, fieldName);
			if(accessLevel == 'Read')
			{
				return getReadOnlyField(fieldName,label,value);
			}
			else if(accessLevel == 'None')
			{
				return null;
			}
			
			var valueDiv    = $('<div class="votePickerValue">' + value + '</div>').appendTo(picklistDiv);
			var votePickListContainer = $('<div class="votePickerContainer" />').appendTo(picklistDiv);
			var selectorDiv = $('<div/>').appendTo(votePickListContainer);
			var buttonsDiv  = $('<div class="votePickerButtonWrapper" />').appendTo(votePickListContainer);
			$(buttonsDiv).hide();
			
			var selectorGrid = new ACEDataGrid($(selectorDiv) ,{  maxNumberOfRows:10, 
				autoHeight: true, 
				multiSelect: true,
				forceFitColumns: true, 
				explicitInitialization: true,
				editable: true,																					
				headerRowHeight: 40
			});
			
			var picklistValues = [];
			
			ACE.Remoting.call
			(
				ACE.Namespace.CORE_APEX + "ACECoreController.getPicklistValues",
				[ objectName, fieldName ],
				function(result, event)
				{
					if (event.status)
					{
						var selectedRows = [];
						for(var x = 0;x < result.length; x++)
						{
							var picklistValue = result[x];
							picklistValue.id = picklistValue.data;
							var currentValues = value.split(';');
							for(var i=0; i < currentValues.length;i++)
							{	
								var curVal = currentValues[i];
								if(picklistValue.data == curVal)
								{
									picklistValue.selected = true;
									selectedRows.push(x);
								}
								else
								{
									picklistValue.selected = false;
								}
							}
							
							if(picklistValue !== undefined)
							{
								picklistValues.push(picklistValue);
							}	                		
						}

						var columns = [	{id: 'region', field: 'data', width: '100', name: 'LOB', sortable: true, fieldType: 'String'}
						];
						
						selectorGrid.setColumns(columns);
						selectorGrid.grid.init();
						selectorGrid.addItems(picklistValues);
						selectorGrid.grid.setSelectedRows(selectedRows);
						$(selectorDiv).hide();
						selectorGridForResize = selectorGrid;
						
						$(valueDiv).click(function ()
						{
							$(buttonsDiv).show(333);
							$(selectorDiv).show(333);
						});
						
						var addButton = $('<button class="confirmButton la-ripple-container" ripple="true">Set</button>').appendTo(buttonsDiv);            		
						var hideButton = $('<button class="cancelButton">Cancel</button>').appendTo(buttonsDiv);
						
						if(canEdit != true)
						{
							addButton[0].disabled = true;							
						}

						$(addButton).on('click', function() 
						{
							var selectedValues = selectorGrid.getSelectedItems();
							
							var selValuesString = null;
							
							if(selectedValues != null  && selectedValues.length > 0)
							{
								selValuesString = '';
								for(var i = 0; i < selectedValues.length;i++)
								{   							
									selValuesString += selectedValues[i].data + ';';
								}	
							}

							// removing trailing semicolon
							if (selValuesString.slice(-1) == ";")
							{
								selValuesString = selValuesString.slice(0, -1); 
							}
							
							accountVotingPeriodToUpdate[fieldName] = selValuesString;
							$(saveButton).show(333);
							
							nonLOBSectionHasChanges = true; 

							$(buttonsDiv).hide(333);
							if(selValuesString != null)
							{
								$(valueDiv).text(selValuesString);
							}	
							else
							{
								$(valueDiv).text('click here to add values');
							}
							$(valueDiv).show(333);
							$(selectorDiv).hide();
						});
						
						$(hideButton).on('click', function() 
						{
							$(selectorDiv).hide(333);
							$(buttonsDiv).hide(333);
							$(valueDiv).show(333);
						});
					}
				}            
			);
			
			$(component).append(picklistDiv);
		}

		//  ==================================================================
		//  Method         : createNotesInput
		//  Description    : create a text area input
		//  @param         : component,inputId,objectName,label,value,vote,laType, canEdit
		//  ==================================================================
		Vote.prototype.createNotesInput = function(component,inputId,label,laType,canEdit)
		{		
			var value = this.dataprovider[inputId];			
			if(value == null)
			{
				value = '';
			}
			
			var accessLevel = getAccessLevel('Vote__c', inputId);
			
			if(accessLevel == 'None')
			{
				return null;
			}
			
			var thisInput = $('<LA-' + laType + ' id="' + inputId + '" value="' + value + '"> </LA-' + laType + '>');

			if(canEdit != true || accessLevel == 'Read')
			{
				thisInput[0].disabled = true;
			}
			
			var _this = this;

			thisInput.on('change', function() 
			{
				$(saveButton).show(333);
				enhancedLOBHasChanges = true; 
			});
			
			$(component).append(thisInput);

			this.inputs.push(thisInput);

			return thisInput;
		}

		//  ==================================================================
		//  Method         : getAttachmentsGrid
		//  Description    : create attachment grid
		//  @param         : component
		//  ==================================================================
		function getAttachmentsGrid(component)
		{
			var votesSectionHeader = $('<div class="votesSectionHeader" />').appendTo(component);
			$('<div class="votesSectionHeaderLabel">Vote Files</div>').appendTo(votesSectionHeader);
			var attachmentGridButtonWrapper = $('<div class="attachmentGridButtonWrapper" />').appendTo(votesSectionHeader);
			
			var votesSectionInnerWrapper = $('<div class="votesSectionInnerWrapper" />').appendTo(component);
			var votesAttachmentGridWrapper = $('<div class="votesAttachmentGridWrapper" />').appendTo(votesSectionInnerWrapper);

			var filesDiv = $('<div id="filesDiv"/>').appendTo(votesAttachmentGridWrapper);
			

			var filesGrid = new ACEDataGrid($(filesDiv) ,{  maxNumberOfRows:4, 
				autoHeight: true, 
				rowHeight: 25,
				forceFitColumns: true, 
				explicitInitialization: true,
				editable: true,																					
			});
			
			function defaultFormatter(row, cell, value, columnDef, dataContext)
			{
				if (value == null) value = '';  
				
				return value.toString();
			}
			
			attatColumns = [{id: "checkboxSelectorTop",formatter: "CheckBoxSelectorItemRenderer",field: "CheckBoxSelectorItemRenderer",width: 25,order:0,maxWidth: 25,minWidth: 25,sortable: false},
							{id: 'Name', field: 'Name', width: '80', name: 'File Name', sortable: true, fieldType: 'String', formatter: defaultFormatter},
							{id: 'ContentType', field: 'ContentDocument.FileType', width: '10', name: 'Document Type', sortable: true, fieldType: 'String'},
							{id: 'CreatedDate', field: 'createDateSting', width: '10', name: 'Date Added', sortable: true, fieldType: 'String'}
							];
			
			filesGrid.setColumns(attatColumns);
			filesGrid.grid.init();
			
			if(accountVotingPeriod.Id !== undefined && accountVotingPeriod.Id != null)		
			{
				var files = [];
				
				ACE.Remoting.call("VoteInformationController.getAttachmentsForGrid", [accountVotingPeriod.Id], function(result,event) 
				{ 
					if(event.status)
					{		
						files = result;
						
						for(var x = 0; x < files.length;x++)
						{
							var thisFile = files[x];
							thisFile.id = thisFile.Id;
							thisFile.createDateSting = new Date(thisFile.ContentDocument.CreatedDate).toLocaleDateString();
							thisFile.Name = '<a target="_blank" href="' + serverURL + '/sfc/servlet.shepherd/version/download/' + thisFile.ContentDocument.LatestPublishedVersionId + '" title="click to launch document">' + thisFile.ContentDocument.Title + '</a>';
						}

						filesGrid.addItems(files);
						gridInit = true;					
					}
					else
					{			
						ACE.FaultHandlerDialog.show({title:'Unexpected Error Occured on Save',message : result, error : 'Please review you data and try again'});
					} 
				});
			}
			
			var acceptAttr = acctVoteInfo.allowedAttachmentExtensions && acctVoteInfo.allowedAttachmentExtensions.length > 0 ? 
							 acctVoteInfo.allowedAttachmentExtensions.join(',') : null;
			var acceptStr = acceptAttr ? 'accept="' + acceptAttr + '"' : 'accept=""';
			var inputFile = $('<input type="file" ' + acceptStr + ' style="display:none"/>').appendTo(attachmentGridButtonWrapper);
			var selectFileButton = $('<button class="attachmentButton">Attach Vote File(s)</button>').appendTo(attachmentGridButtonWrapper);
			var deleteAttachmentsButton = $('<button class="removeAttachmentButton">Remove Attachment(s)</button>').appendTo(attachmentGridButtonWrapper);

			if(acctVoteInfo.canEdit != true)
			{
				selectFileButton[0].disabled = true;
				deleteAttachmentsButton[0].disabled = true;
			}
			
			selectFileButton.on("click", function() 
			{
				inputFile.click();
			});

			inputFile.on("input", function() 
			{
				var filename = inputFile[0].files[0].name;

				var addToGrid = {
					id: attachmentCount,
					Name: filename,
					ContentType: 'New File'
				};
				
				//If the file doesnt have the allowed file extension
				if(Array.isArray( acctVoteInfo.allowedAttachmentExtensions ) && 
				acctVoteInfo.allowedAttachmentExtensions.length > 0)
				{
					var fileExt = '.' + filename.split('.').pop();
					if(!acctVoteInfo.allowedAttachmentExtensions.includes(fileExt))
					{
						console.log('Invalid file extension found!');
						ACE.FaultHandlerDialog.show({title:'Error on loading attachments',
							message : acctVoteInfo.allowedAttachmentExtensionsWarning, error : acctVoteInfo.allowedAttachmentExtensionsWarning});
						inputFile.val("");
						return;
					}
				}

                var newFile = inputFile[0].files[0];
				fileMap[attachmentCount] = newFile;

				filesGrid.addItems(addToGrid);
				attachmentCount++;
				$(saveButton).show(333);
				nonLOBSectionHasChanges = true;

				//Clear the input buffer after processing
				//inputFile[0].val("");
				inputFile[0].value = "";
			});	

			/*
			attachmentParent = new sforce.SObject("Vote_Attachment__c");
			var attachment = new sforce.SObject("Attachment");
			
			var thisFileForUpload = '';
			var _fileReader = new FileReader();
			
			selectFileButton.on("click", function() 
			{
				var saveResult = sforce.connection.upsert("Id", [attachmentParent]);			
				for (var x = 0; x < saveResult.length; x++) 
				{ 
					var res = saveResult[x];
					if(res.success =='false')
					{	
						ACE.FaultHandlerDialog.show({title:'Error on loading attachments',message : res.errors.message, error : 'Please review you data and try again'});
					}
					else
					{
						attachmentParent.Id = res.id;
					}
				}	
				
				attachment.ParentId = attachmentParent.Id;
				
				inputFile.click();
			});
			
			inputFile.on("change", function()
			{
				thisFileForUpload = inputFile.get(0).files[0];

				attachment.Name = thisFileForUpload.name;
				attachment.ContentType = thisFileForUpload.type;
				
				_fileReader.readAsDataURL(thisFileForUpload);
			});

			var isReady = false;
			
			_fileReader.onload = function(e)
			{
				//Get the base64 encoded data... the return looks something like this:
				//text/plain;base64,<data>
				
				if (e.target.result != null)
				{
					attachment.Body = e.target.result.substring(e.target.result.lastIndexOf(",") + 1); 

					var saveResult = sforce.connection.upsert("Id", [attachment]);	
					var addToGrid = [];		
					for (var x = 0; x < saveResult.length; x++) 
					{ 
						var res = saveResult[x];
						if(res.success =='false')
						{	
							ACE.FaultHandlerDialog.show({title:'Error on loading attachments',message : res.errors.message, error : 'Please review you data and try again'});
						}
						else
						{
							addToGrid.push({
												id: res.id,
												Name: '<a target="_blank" href="' + serverURL + '/servlet/servlet.FileDownload?file=' + res.id + '" title="click to launch document">' + attachment.Name + '</a>',
												ContentType: attachment.ContentType
										});
							lastAttachmentId = res.id;
							lastAttachmentName = attachment.Name;
							filesGrid.addItems(addToGrid);
							
							$(saveButton).show(333);
						}
					}	
					
					getAttachmentList(filesGrid);		
				}
				else
				{
					attachment.Body = '';
				}     
			}
			*/
			
			$(deleteAttachmentsButton).on('click', function() 
			{
				var selectedAttachments = filesGrid.getSelectedItems();
				var attsToDelete = [];
				
				if(selectedAttachments && selectedAttachments.length > 0)
				{	
					var deletedLastAttachment = false;				
					for(var i = 0; i < selectedAttachments.length;i++)
					{
						var selectedAtt = selectedAttachments[i];
						attsToDelete.push(selectedAtt.id);		
						filesGrid.dataProvider.deleteItem(selectedAtt.id);
						deletedLastAttachment = deletedLastAttachment || 
								(selectedAtt.ContentDocument && selectedAtt.ContentDocument.LatestPublishedVersionId &&
								selectedAtt.ContentDocument.LatestPublishedVersionId == accountVotingPeriod.Last_Attachment_Id__c);
					}

					filesGrid.dataProvider.refresh();
					
					var attsToDeleteFromDatabase = attsToDelete.filter(function(id)
					{
						// Remove the file from the map so we dont save it after it has been deleted
						if (id.length !== 18)
						{
							delete fileMap[id];
						}
						return id.length === 18;
					});

					if (attsToDeleteFromDatabase && attsToDeleteFromDatabase.length > 0)
					{
						var delAttachmentResult = sforce.connection.deleteIds(attsToDelete);
						for(var y=0; y < delAttachmentResult.length; y++)
						{
							var res = delAttachmentResult[y];
							if(res.success == 'false')
							{
								ACE.FaultHandlerDialog.show({title:'Error on retrieving attachments',message : res.errors.message, error : 'Please review you data and try again'});
							}
															
						}
					}

					//if the Last_Attachment_Id__c was deleted, move the id to the most recent attachment
					if(deletedLastAttachment)
					{
						//Null out the assignment
						accountVotingPeriod.Last_Attachment_Id__c = null;
						accountVotingPeriod.Last_Attachment_Name__c = null;

						var files = filesGrid.dataProvider.getItems();
						var contentId = null;
						var newLinkId = null;
						//If we have any items left, we should assign the most recently saved attachment
						$.each(files, function(index, file)
						{
							if(file.ContentDocument && file.ContentDocument.LatestPublishedVersionId)
							{
								contentId = file.ContentDocument.Id;
								newLinkId = file.Id;
								accountVotingPeriod.Last_Attachment_Id__c = file.ContentDocument.LatestPublishedVersionId;
								accountVotingPeriod.Last_Attachment_Name__c = file.ContentDocument.Title;
							}
						});
						
						if(accountVotingPeriod.Id)
						{
							ACE.Remoting.call("VoteInformationController.reparentFile", [contentId, accountVotingPeriod.Id, newLinkId],function(result,event)
							{
								if(!event.res)
								{
									console.log('Quack!')
								}
							});
						}
					}

					//getAttachmentList(filesGrid);
				}
			});	
		}

		//  ==================================================================
		//  Method         : getAttachmentList
		//  Description    : retrieve the attachments that were saved
		//  @param         : filesGrid
		//  ==================================================================
		function getAttachmentList(filesGrid)
		{
			if(accountVotingPeriod.Id != null)
			{
				ACE.Remoting.call("VoteInformationController.getAttachmentsForGrid", [accountVotingPeriod.Id], function(result,event) 
				{ 
					if(event.status)
					{	
						var atts = result;
						if(atts.length > 0)
						{
							for(var x = 0; x < result.length;x++)
							{
								var thisFile = result[x];
								thisFile.id = thisFile.Id;
								thisFile.createDateSting = new Date(thisFile.ContentDocument.CreatedDate).toLocaleDateString();
								thisFile.Name = '<a target="_blank" href="' + serverURL + '/sfc/servlet.shepherd/version/download/' + thisFile.ContentDocument.LatestPublishedVersionId + '" title="click to launch document">' + thisFile.ContentDocument.Title + '</a>';
							}									
						}	
						
						if(gridInit)
						{
							filesGrid.clear();
							filesGrid.setItems(atts);
							filesGrid.grid.render();
						}
						else
						{																	
							filesGrid.setColumns(attatColumns);
							filesGrid.grid.init();
							filesGrid.setItems(atts);
							gridInit = true;
						}
					}
					else
					{			
						ACE.FaultHandlerDialog.show({title:'Unexpected Error Occured where getting Attachments',message : result, error : 'Please review your data and try again'});
					} 	
				});
			}
		}

		/** 
		 * Name        : vote
		 * Description : vote class
		*/
		function Vote(component, vote, isAcctVote, defaultValues, isEnhancedVote, applyNoVote)
		{
			var _this = this; 

			// set up target rank defaults
			if (vote.isTeamVote && acctVoteInfo.regionLOBToTargetRankDefault != null)
			{
				if (vote.vote.Target_Rank__c == null) 
				{
					if (acctVoteInfo.regionLOBToTargetRankDefault[vote.vote.Region__c] != null)
					{
						vote.vote.Target_Rank__c = acctVoteInfo.regionLOBToTargetRankDefault[vote.vote.Region__c];
					}
				}
			}

			// handle the apply no vote checkbox clicks.
			var initialRank = (vote.vote.Rank_List__c == null ? null : vote.vote.Rank_List__c); 
			$(applyNoVote).click(function()
			{
				if ($(applyNoVote).is(':checked'))
				{
					if (_this.rankPicker.val() == null || _this.rankPicker.val() == '')
					{
						_this.rankPicker.val('No vote').change();
					}
				}
				else 
				{
					if (_this.rankPicker.val() == 'No vote' && initialRank != 'No vote')
					{
						_this.rankPicker.val(null).change();
					}
				}
			});

			// todo above

			if(isAcctVote === undefined)
			{
				isAcctVote = false;
			}	
			
			this.dataprovider = vote.vote;
			this.contacts = this.dataprovider.Vote_Contacts__r;

			var contNames = '';
			if(this.contacts != null)
			{
				contNames = this.contacts[0].Contact__r.Name;
				if(this.contacts.length > 1)
				{
					contNames += ' +' + (this.contacts.length - 1 )
				}
			}

			this.contactNames = contNames;
			this.inputs = [];
		
			var regionWidth = '130px;padding-right:5px';
			var columnWidth = '103px;padding-left:20px';
			
			if(isAcctVote)
			{			
				var accountVoteGridRow = $('<div class="accountVoteGridRow" />').appendTo(component);
				var rowColumnOne = $('<div class="rowColumn labelColumn" />').appendTo(accountVoteGridRow);
				var rowLabelWrapper = $('<div class="rowLabelWrapper"><div class="accountVoteGridRowLabel">' +  vote.region + '</div></div>').appendTo(rowColumnOne);
				var rowColumn = $('<div class="rowColumn" />').appendTo(accountVoteGridRow);
				var rowInputWrapperOne = $('<div class="rowInputWrapper" />').appendTo(rowColumn);
				var rowInputWrapperTwo = $('<div class="rowInputWrapper" />').appendTo(rowColumn);

				this.createVoteSeriesPicklist(rowInputWrapperOne,'Vote_Series__c',defaultValues.voteSeriesPicklist, acctVoteInfo.canEdit);

				this.createVoteInput(rowInputWrapperTwo,'Label__c',isAcctVote,acctVoteInfo.canEdit);

				var rowColumnTwo = $('<div class="rowColumn" />').appendTo(accountVoteGridRow);
				var rowColumnThree = $('<div class="rowColumn" />').appendTo(accountVoteGridRow);

				this.createVotePicklist(rowColumnTwo,'Eval1__c','','Eval2__c', acctVoteInfo.canEdit);		
				this.createVotePicklist(rowColumnThree,'Eval2__c','','Eval1__c', acctVoteInfo.canEdit);
			}
			else if(isEnhancedVote)		
			{
				var voteName = vote.parentName;

				if(voteName == null)
				{
					voteName = vote.region;
				}

				$('<td class="vote-table-cell">' + voteName + '</td>').appendTo(component);

				if(vote.parentName == null)
				{
					var rankColumn = $('<td class="vote-table-cell" />').appendTo(component);
					var rankColumnValue = $('<div class="gridBodyContainer tabSectionGridRow rowColumn rowInputWrapper" />').appendTo(rankColumn);
					
					this.createVoteInput(rankColumnValue,'Rank__c',false,acctVoteInfo.canEdit);
					var rankSlashDiv = $('<div class="outOf">/</div>').appendTo(rankColumnValue);
					this.createVoteInput(rankColumnValue,'Rank_Out_Of__c',false,acctVoteInfo.canEdit);
		
					var tierColumn = $('<td class="vote-table-cell" />').appendTo(component);
					var tierColumnValue = $('<td class="gridBodyContainer tabSectionGridRow rowColumn rowInputWrapper" />').appendTo(tierColumn);
					
					this.createVoteInput(tierColumnValue,'Tier__c',false,acctVoteInfo.canEdit);
					var tierSlashDiv = $('<td class="outOf">/</div>').appendTo(tierColumnValue);
					this.createVoteInput(tierColumnValue,'Tier_Out_Of__c',false,acctVoteInfo.canEdit);
				}
				else
				{
					var tierMultplier = 1;

					// set up the tier multiplier.
					if (vote.vote.Region__c != null && acctVoteInfo.regionToTier != null && acctVoteInfo.tierMapping != null)
					{
						var tier = acctVoteInfo.regionToTier[vote.vote.Region__c];
						for (var i = 0; i < acctVoteInfo.tierMapping.length; i++)
						{
							if (acctVoteInfo.tierMapping[i].name == tier)
							{
								tierMultplier = acctVoteInfo.tierMapping[i].points;
							}
						}
					}

					var rankInputTD = $('<td class="vote-table-cell"></td>').appendTo(component);
					this.rankPicker = this.attachPicklistToVote(rankInputTD,rankPicklistMaster,rankPicklistValues,'Rank_List__c',false);
					
					var targetRankInputTD = $('<td class="vote-table-cell"></td>').appendTo(component);
					this.targetRankPicker = this.attachPicklistToVote(targetRankInputTD,targetRankPicklistMaster,targetRankPicklistValues,'Target_Rank__c',false);

					var pointsInputTD = $('<td class="vote-table-cell"></td>').appendTo(component);
					this.pointsInput = this.createVoteInput(pointsInputTD,'Points__c',false,acctVoteInfo.canEditPoints);
					
					var targetPointsInputTD = $('<td class="vote-table-cell"></td>').appendTo(component);
					this.targetPointsInput = this.createVoteInput(targetPointsInputTD,'Target_Points__c',false,acctVoteInfo.canEditPoints);

					// default target rank... needs to autpopulate points
					var targetRankVal = this.targetRankPicker.val();
					var targetRankPointValue = acctVoteInfo.rankPointsMap[targetRankVal];
					if(targetRankPointValue == null)
					{
						targetRankPointValue = 1;
					}

					if (targetRankVal == '' || targetRankVal == null)
					{
						$(this.targetPointsInput).val('');
					}
					else 
					{
						$(this.targetPointsInput).val(tierMultplier * targetRankPointValue);
					}
					
					var __this = this;

					if (acctVoteInfo.pointsEnabled)
					{
						$(this.rankPicker).change(function()
						{
							var rankVal = $(this).val();

							var rankPointValue = acctVoteInfo.rankPointsMap[rankVal];
							if(rankPointValue == null)
							{
								rankPointValue = 1;
							}

							if (rankVal == '' || rankVal == null)
							{
								$(__this.pointsInput).val('');
							}
							else 
							{
								$(__this.pointsInput).val(tierMultplier * rankPointValue);
							}
						});

						$(this.targetRankPicker).change(function()
						{
							var targetRankVal = $(this).val();

							var targetRankPointValue = acctVoteInfo.rankPointsMap[targetRankVal];
							if(targetRankPointValue == null)
							{
								targetRankPointValue = 1;
							}

							if (targetRankVal == '' || targetRankVal == null)
							{
								$(__this.targetPointsInput).val('');
							}
							else 
							{
								$(__this.targetPointsInput).val(tierMultplier * targetRankPointValue);
							}
						});
					}

					var notesInputTD = $('<td class="vote-table-cell votes-note-column"></td>').appendTo(component);
					var notesValue = this.dataprovider['Notes__c'];
					var addNotesAddClass = false;
					if(notesValue == null)
					{
						notesValue = '';
						addNotesAddClass = true;
					}

					var notesInputContainer = $('<span title="' + notesValue + '">' + notesValue + '</span>').appendTo(notesInputTD);				
					
					if(addNotesAddClass)
					{
						$(notesInputContainer).addClass('vote-notes-add');					
					}
					else
					{
						$(notesInputContainer).addClass('votes-notes-text');
					}

					var _this = this;
					$(notesInputContainer).click(function()
					{
						_this.showNotesAndContactsModal(acctVoteInfo.canEdit, notesInputContainer, this.addContactsButton);

						if(initGrid)
						{
							initGrid = false;
							contactLookupAutoCompleteDataGrid.grid.init();
						}	
						else
						{
							contactLookupAutoCompleteDataGrid.grid.resizeCanvas();
						}
						contactLookupAutoCompleteDataGrid.grid.invalidate();
					});

					var contactsInput = $('<td class="vote-table-cell votes-account-contact-column"></td>').appendTo(component);		
					this.addContactsButton = $('<span/>').appendTo(contactsInput);
					
					if(this.contactNames.length > 0)
					{
						$(this.addContactsButton).text(this.contactNames);
						$(this.addContactsButton).prop('title', this.contactNames);
						$(this.addContactsButton).addClass('votes-contacts-list');
					}
					else
					{
						$(this.addContactsButton).addClass('vote-notes-add');
						$(this.addContactsButton).prop('title', '');					
					}

					$(this.addContactsButton).click(function()
					{
						_this.showNotesAndContactsModal(acctVoteInfo.canEdit, notesInputContainer, _this.addContactsButton);

						if(initGrid)
						{
							initGrid = false;
							contactLookupAutoCompleteDataGrid.grid.init();
						}	
						else
						{
							contactLookupAutoCompleteDataGrid.grid.resizeCanvas();
						}
					});
				}	
			}
			else if(vote.hideLabel != true)
			{
				if(vote.parentName != null)
				{
					regionWidth = '170px;padding-right:10px';
					columnWidth = '65px';
				}
				else if(vote.LOBId == null)
				{
					regionWidth = '130px;padding-right:10px';
					columnWidth = '70px';
				}
							
				if(vote.parentName != null)
				{				
					$('<div class="rowColumn labelColumn"><div class="rowLabelWrapper"><div class="accountVoteGridRowLabel">' + vote.parentName + '</div></div></div>').appendTo(component);
				}	

				if(vote.region != null)
				{
					$('<div class="rowColumn labelColumn"><div class="rowLabelWrapper"><div class="accountVoteGridRowLabel">' + vote.region + '</div></div></div>').appendTo(component);
				}
				
				var rankColumn = $('<div class="rowColumn" />').appendTo(component);
				var rankColumnValue = $('<div class="rowInputWrapper" />').appendTo(rankColumn);
				var rankSlashDiv = $('<div class="outOf">/</div>').appendTo(rankColumn);
				var rankColumnOutOf = $('<div class="rowInputWrapper" />').appendTo(rankColumn)

				var tierColumn = $('<div class="rowColumn" />').appendTo(component);
				var tierColumnValue = $('<div class="rowInputWrapper" />').appendTo(tierColumn);
				var tierSlashDiv = $('<div class="outOf">/</div>').appendTo(tierColumn);
				var tierColumnOutOf = $('<div class="rowInputWrapper" />').appendTo(tierColumn);
		
				this.createVoteInput(rankColumnValue,'Rank__c',false,acctVoteInfo.canEdit);
				this.createVoteInput(rankColumnOutOf,'Rank_Out_Of__c',false,acctVoteInfo.canEdit);
					
				this.createVoteInput(tierColumnValue,'Tier__c',false,acctVoteInfo.canEdit);
				this.createVoteInput(tierColumnOutOf,'Tier_Out_Of__c',false,acctVoteInfo.canEdit);
							
				if(vote.parentName != null)
				{
					var pointsColumn = $('<div class="rowColumn" />').appendTo(component);
					var pointsColumnValue = $('<div class="rowInputWrapper" />').appendTo(pointsColumn);				
					this.createVoteInput(pointsColumnValue,'Points__c',false,acctVoteInfo.canEdit);
				}					
			}	
		}

		Vote.prototype.createVoteInput= function(component,inputId,isAcctVote,canEdit)	
		{
			var value = this.dataprovider[inputId];
			if(value == null)
			{
				value = '';
			}

			var accessLevel = getAccessLevel('Vote__c', inputId);

			if(accessLevel != 'Edit')
			{
				canEdit = false;
			}

			var thisInput;
			
			if(value === undefined)
			{
				value = '';
			}

			if(isAcctVote)
			{			
				thisInput = $('<input id="' + inputId + '" type="text" class="text-input-left-padding" value="' + value + '"></input>');
			}
			else if(inputId == 'Points__c')
			{
				thisInput = $('<input id="' + inputId + '" type="text" class="text-input-left-padding" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  value="' + value + '"></input>');
			}
			else
			{
				thisInput = $('<input id="' + inputId + '" type="text" class="text-input-left-padding" value="' + value + '"></input>');
			}	
		
			if(canEdit != true)
			{
				thisInput[0].disabled = true;
			}

			$(thisInput).change(function() 
			{
				nonLOBSectionHasChanges = true; 

				$(saveButton).show(333);
			});

			$(thisInput).appendTo(component);

			this.inputs.push(thisInput);
			
			return thisInput;
		}

		Vote.prototype.createNotesContactsInput = function(canEdit)
		{
			var _this = this;

			$(notesInputDiv).empty();

			var thisNotesInput = this.createVoteNotesInput(notesInputDiv, canEdit);
			lastNotesInput = thisNotesInput;

			$(contactLookupAutoCompleteDiv).empty();

			this.createContactLookup(contactLookupAutoCompleteDiv);

			var contactDataProv = [];

			if(this.contacts != null)
			{
				for(var x=0;x<this.contacts.length;x++)
				{
					var cont = this.contacts[x];
					if(cont.Contact__r != null)
					{
						contactDataProv.push({id : cont.Id, contactId : cont.Contact__c, Name : cont.Contact__r.Name});
					}
					else if(cont.id == null)
					{
						contactDataProv.push({id : x, contactId : cont.id, Name : cont.Name});
					}
					else
					{
						contactDataProv.push({id : cont.id, contactId : cont.id, Name : cont.Name});
					}
				}
			}
			setGridData(contactDataProv);
		}

		function setGridData(data)
		{
			contactLookupAutoCompleteDataGrid.grid.resetActiveCell();
			contactLookupAutoCompleteDataGrid.dataProvider.beginUpdate();
			contactLookupAutoCompleteDataGrid.dataProvider.setItems(data, 'id');
			contactLookupAutoCompleteDataGrid.dataProvider.endUpdate();
			contactLookupAutoCompleteDataGrid.grid.resizeCanvas();
			contactLookupAutoCompleteDataGrid.grid.invalidate();
		}

		$(modalfooterConfirmButton).click(function()
		{
			var _this = lastVoteThis;
			var notesInput = lastNotesInput;

			$(modalThisNotesContainer).text(notesInput.val());
			$(modalThisNotesContainer).prop('title', notesInput.val());

			_this.inputs.push(notesInput);
			_this.contactIds = [];
			_this.dataGridItems = contactLookupAutoCompleteDataGrid.grid.getData().getItems();		

			_this.dataprovider['Notes__c'] = $(notesInput).val();

			if(_this.dataprovider['Notes__c'] != null && _this.dataprovider['Notes__c'].length > 0)
			{				
				$(modalThisNotesContainer).removeClass('vote-notes-add');
				$(modalThisNotesContainer).addClass('votes-notes-text');
			}
			else
			{		
				$(modalThisNotesContainer).addClass('vote-notes-add');
				$(modalThisNotesContainer).removeClass('votes-notes-text');
			}

			for(var x=0;x<_this.dataGridItems.length;x++)
			{
				var thisVoteContact = _this.dataGridItems[x];
				var voteExists = false;
				if(_this.contacts != null)
				{
					for(var i=0;i<_this.contacts.length;i++)
					{
						var existingContactVote = _this.contacts[i];
						var thisVoteContactId = thisVoteContact.id;
						var existingVoteContactId = existingContactVote.Id;

						if(thisVoteContactId.indexOf('003') == -1 && thisVoteContactId == existingVoteContactId)
						{
							voteExists = true;
						}
					}
				}

				if(!voteExists)
				{
					if(_this.contacts == null)
					{
						_this.contacts = [];
					}

					var thisVoteContactId = thisVoteContact.id;

					if(thisVoteContactId.indexOf('003') == -1)
					{
						_this.contactIds.push(thisVoteContact.contactId);
					}
					else
					{
						_this.contactIds.push(thisVoteContact.id);
					}
					
					addToContacts(_this.contacts,thisVoteContact,thisVoteContact.id);
				}				
			}

			_this.contacts = [];

			for(var x=0;x<_this.dataGridItems.length;x++)
			{
				var thisVoteContact = _this.dataGridItems[x];

				addToContacts(_this.contacts,thisVoteContact,thisVoteContact.id);
			}

			if(_this.contacts != null && _this.contacts.length > 0 && _this.dataGridItems != null && _this.dataGridItems.length > 0)
			{
				var buttonText = _this.dataGridItems[0].Name;
				if(_this.contacts.length > 1)
				{
					buttonText += ' +' + (_this.contacts.length - 1);
				}	
				$(_this.addContactsButton).text(buttonText);
				$(_this.addContactsButton).prop('title', buttonText);

				$(_this.addContactsButton).removeClass('vote-notes-add');
				$(_this.addContactsButton).addClass('votes-contacts-list');
			}

			$(saveButton).show(333);

			dialog.dialog('close');
			return false;
		});

		$(modalfooterCancelButton).click(function()
		{
			dialog.dialog('close');
			return false;
		});
		
		Vote.prototype.createVoteNotesInput = function(notesInputDiv, canEdit)
		{
			var value = this.dataprovider['Notes__c'];			
			if(value == null)
			{
				value = '';
			}

			var accessLevel = getAccessLevel('Vote__c', 'Notes__c');

			if(accessLevel != 'Edit')
			{
				canEdit = false;
			}
							
			var thisInput = $('<textarea id="Notes__c" class="vote-notes">' + value + '</textarea>').appendTo(notesInputDiv);

			if(value.length == 0)
			{
				$(modalThisNotesContainer).addClass('vote-notes-add');
				$(modalThisNotesContainer).removeClass('votes-notes-text');
			}
			else
			{
				$(modalThisNotesContainer).removeClass('vote-notes-add');
				$(modalThisNotesContainer).addClass('votes-notes-text');
			}

			if(canEdit != true)
			{
				thisInput[0].disabled = true;
			}

			var _this = this;

			$(thisInput).change(function() 
			{
				$(saveButton).show(333);
				enhancedLOBHasChanges = true; 
			});

			return thisInput;
		}

		function addUniqueToArray(list,listIds,item) 
		{
			var isARR = $.inArray(item.id, listIds);
			if(isARR == -1)
			{
				list.push(item);
			}
		}

		Vote.prototype.createContactLookup = function(component)
		{
			var thisInput = $('<LA-Input placeholder="Search for Vote Contacts" value="" label=""> </LA-Input>').appendTo(component);
			var columns = [{'id':'FirstName','field':'FirstName','Style':'color: #19198C;','apexType':"ACECoreController.Column"},
							{'id':'LastName','field':'LastName','Style':'color: #19198C;','apexType':"ACECoreController.Column"},
							{'id':'Title','field':'Title','Style':'color: #19198C;','apexType':"ACECoreController.Column"}];
			
			var lookupConfig = {};
			lookupConfig['sobjectName'] = 'Contact';
			lookupConfig['additionalQueryFields'] = ['Name','FirstName','LastName','Title'];
			lookupConfig['additionalWhereClause'] ="AccountId = '" + acctVoteInfo.accountId + "' and T1C_Base__Inactive__c = false";
			lookupConfig['columnList'] = columns;
			var _this = this;
			setTimeout(function()
			{
				autoComplete = new SearchAutoComplete($(thisInput),
				{
					mode: "CUSTOM:ACE.CoreWrappers.Votes.Contact",
					maintainSelectedItem: false,
					coverageOnly: false,
					additionalQueryFields: ['Name','FirstName','LastName','Title'],
					additionalWhereClause: "AccountId = '" + acctVoteInfo.accountId + "' and T1C_Base__Inactive__c = false",
					config: lookupConfig,
					select: function(item)
					{
						console.log(item);
						// Add to slick grid
						if(item != null)
						{
							var addingItem = {id:item.Id,Name:item.Name};
							var gridlist = contactLookupAutoCompleteDataGrid.grid.getData().getItems();
							var listIds = [];
							for(var x=0; x<gridlist.length;x++)
							{
								listIds.push(gridlist[x].id);
							}

							addUniqueToArray(gridlist,listIds,addingItem);

							setGridData(gridlist);						
						}
					}		
				});						
			}, 100);
		}

		function addToContacts(list,item,contactId)
		{
			if(list != null)
			{			
				list.push(item);

				list = $.unique(list);
			}
		}

		function createRegionPicklist(periodLeft, acctVoteInfo)
		{
			var regionPickerDiv = $('<div class="voteControl quaterPicker"/>').appendTo(periodLeft);	
			$('<div class="voteControlLabel">Vote From Region</div>').appendTo(regionPickerDiv);

			var picklistValues = [];

			for(var x=0;x<acctVoteInfo.regionPicklistValues.length;x++)
			{
				var pl = acctVoteInfo.regionPicklistValues[x];
				picklistValues.push({label:pl.label,data:pl.value,defaultSelected:pl.isDefault});
			}

			regionValue = new ACEMenuButton($(regionPickerDiv),{ dataProvider : picklistValues});

			regionValue.on('click', function()
			{
				accountVotingPeriodToUpdate['Vote_From_Region__c'] = regionValue.val();
			});
		}

		//  ==================================================================
		//  Method         : generateHTML
		//  Description    : draw the html for the vote page
		//  @param 		   : 
		//  ==================================================================
		function generateHTML()
		{	
			var _this = this;

			saveButton   = $('<button class="confirmButton">Save</button>').appendTo(footer);
			var cancelButton = $('<button class="cancelButton">Cancel</button>').appendTo(footer);
				
			var accountVoteInfo        = createDiv(formBody,'280px;margin:10px','left','votesTopSection');
			var accountVoteInfoWrapper = $('<div class="votesAccountWrapper" />').appendTo(accountVoteInfo);
			$('<div class="votesAccountName"><div class="votesAccountNameLabel">'+ accountName + '</div></div>').appendTo(accountVoteInfoWrapper);
			if(accountLogo != null)
			{
				$('<div class="votesAccountLogo">' + accountLogo.replace('<img','<img class="accountLogo" ') + '</div>').appendTo(accountVoteInfoWrapper);
			}
			
			var votingFrequency = $('<div class="votesSection" />').appendTo(accountVoteInfo);
			$('<div class="votesSectionHeader"><div class="votesSectionHeaderLabel">Voting Frequency</div></div>').appendTo(votingFrequency);
			
			var periodLeft = createDiv(votingFrequency,'280px;','left','votesRangeSection');
			
			period    = createPicklist(periodLeft,'Period__c','T1C_Base__Account_Voting_Period__c','Voting Period',accountVotingPeriod.Period__c, 'voteControl quaterPicker', acctVoteInfo.canEdit);
			startDate = createDatePicker(periodLeft,'Start_Date__c','T1C_Base__Account_Voting_Period__c','Voting Start',accountVotingPeriod.Start_Date__c,false, acctVoteInfo.canEdit);
			endDate   = createDatePicker(periodLeft,'End_Date__c','T1C_Base__Account_Voting_Period__c','Voting End',accountVotingPeriod.End_Date__c,false, acctVoteInfo.canEdit);
			lastReceived = createDatePicker(periodLeft,'Last_Received__c','T1C_Base__Account_Voting_Period__c','Last Received',accountVotingPeriod.Last_Received__c,false, acctVoteInfo.canEdit);
			if (acctVoteInfo.enableVoteFromRegion)
			{
				voteFromRegion = createPicklist(periodLeft,'Vote_From_Region__c','T1C_Base__Account_Voting_Period__c','Vote From Region',accountVotingPeriod.Vote_From_Region__c, 'voteControl quaterPicker', acctVoteInfo.canEdit);
			}
			
			// if (acctVoteInfo.enableVoteFromRegion)
			// {
			// 	createRegionPicklist(periodLeft, acctVoteInfo);
			// }

			createMultiSelectField(periodLeft, 'Vote_For__c','T1C_Base__Account_Voting_Period__c','Vote For',accountVotingPeriod.Vote_For__c, acctVoteInfo.canEdit);
			
			$(startDate).on('change', function()
			{
				validatDateFields(true, 'Custom', period);
			});
			
			$(endDate).on('change', function()
			{
				validatDateFields(true, 'Custom', period);
			});
			
			var allLOBVoteInfo = createDiv(formBody,'800px','left','votesSection');
			createVoteContainer(allLOBVoteInfo,allLOBVote);	
		
			var	attachmentsDiv = createDiv(formBody,'610px;padding-left:30px;margin:10px','left', 'votesSection');
			getAttachmentsGrid(attachmentsDiv);
					
			var votesTabsDiv = createDiv(formBody,'100%','left','tabsWrapper');
			createLOBTabs(votesTabsDiv,LOBVotes);		
			
			$(saveButton).hide();
			
			$(saveButton).on('click', function() 
			{
				if (!_this.isSaving)
				{
					_this.isSaving = true;
					$(saveButton).prop('disabled', true);

					var recordsToUpsert = [];
					var avpId;

					if(lastAttachmentId !== '' && lastAttachmentName !== '')
					{
						accountVotingPeriodToUpdate.Last_Attachment_Id__c   = lastAttachmentId;
						accountVotingPeriodToUpdate.Last_Attachment_Name__c = lastAttachmentName;
					}

					if(periodSelected === 'Custom')
					{
						accountVotingPeriodToUpdate.Period__c = 'Custom';
					}

					var confirmMsg; 
					var proceedWithSave = true;
					if($(startDate).val() === '' || $(endDate).val() === '' || periodSelected === 'Select One' || $(lastReceived).val() === '')
					{
						confirmMsg = 'Please fill in the required fields.';
						proceedWithSave = false; 
					}
					else if(!checkDateRange())
					{
						confirmMsg = 'Voting End Date does not come after the Voting Start Date';
						proceedWithSave = false; 
					}
					else
					{
						confirmMsg = 'Are you sure you want to save this vote record?';
					}
					
					if(!proceedWithSave)
					{
						ACEConfirm.show
						(
							confirmMsg,
							"Please Confirm",
							[ "OK::confirmButton"],
							function(result)
							{
								_this.isSaving = false;
								$(saveButton).prop('disabled', false);
							}
						);
					}
					else
					{
						ACEConfirm.show
						(
							confirmMsg,
							"Please Confirm",
							[ "Confirm::confirmButton", "Cancel::cancelButton"],
							function(result)
							{
								if (result === "Confirm")
								{
									var votesForUpsert = [];
									var voteWrapperForUpsert = [];
									// voteWrappers.push(tempVoteWrappers); // todo - commenting this out because it breaks things
									var allVoteWrappers = voteWrappers.concat(tempVoteWrappers);




									for(var x=0;x<allVoteWrappers.length;x++)
									{
										var hasValue = false;
										var thisVoteWrap = allVoteWrappers[x];
										var thisVote = thisVoteWrap.dataprovider;
										var voteInputs = thisVoteWrap.inputs;
										
										for(var i=0;i<voteInputs.length;i++)
										{
											thisVoteInput = voteInputs[i];
											var thisVoteInputVal;
											var thisVoteField = $(thisVoteInput).attr('id');

											if(thisVoteInput.isPicklist == true)
											{
												thisVoteInputVal = thisVoteInput.val();
											}	
											else if(thisVoteInput.isRegularPicklist)
											{
												thisVoteInputVal = thisVoteInput[0].value;
												thisVoteField = thisVoteInput.id;
											}								
											else
											{
												thisVoteInputVal = $(thisVoteInput).val();
											}
											
											if((thisVote.Id && thisVoteField && thisVoteField !== "undefined") || (thisVoteField != null && thisVoteInputVal != null && thisVoteInputVal.length > 0))
											{
												hasValue = true;
												thisVote[thisVoteField] = thisVoteInputVal;
											}	
										}

										if(thisVoteWrap.contactIds != null && thisVoteWrap.contactIds.length > 0)
										{
											hasValue = true;
										}

										if(hasValue)
										{
											if(thisVote.Vote_Contacts__r != null)
											{
												delete thisVote.Vote_Contacts__r;
											}

											thisVoteWrap.vote = thisVote;
											var thisVoteForUpsert = {vote : thisVote, contactIds : thisVoteWrap.contactIds};
											voteWrapperForUpsert.push(thisVoteForUpsert);
										}
									}

									ACE.Remoting.call("VoteInformationController.saveVotes", [accountVotingPeriodToUpdate, voteWrapperForUpsert, delContacts], function(result,event)
									{ 
										if(event.status)
										{
											if(result.errorMessage.length > 0)
											{
												if (result.errorMessage.startsWith("duplicate value found:"))
												{
													var customMessage = acctVoteInfo.duplicateErrorMessage ? acctVoteInfo.duplicateErrorMessage : result.errorMessage;
													ACE.FaultHandlerDialog.show({title:'Unexpected Error Occured on Save',message : customMessage, error : 'Please review you data and try again'});
												}
												else
												{
													ACE.FaultHandlerDialog.show({title:'Unexpected Error Occured on Save',message : result.errorMessage, error : 'Please review you data and try again'});
												}
												_this.isSaving = false;
												$(saveButton).prop('disabled', false);
											}
											
											if(result.acctVoteId != null)
											{
												avpId = result.acctVoteId;
												saveFiles(fileMap,avpId).promise().done(function()
												{																				
													if(ACEUtil.getURLParameter('Id') == null)
													{
														location.href = ACE.Salesforce.domain + '/apex/ACECoreWrapper?path=ACE.CoreWrappers.Votes&Id=' +  avpId;
													}
													else
													{
														location.href = $(location).attr('href'); 
													}																		
												});
											}
										}
										else
										{
											ACE.FaultHandlerDialog.show({title:'Unexpected Error Occured on Save',message : result, error : 'Please review you data and try again'});
											_this.isSaving = false;
											$(saveButton).prop('disabled', false);
										}
									});	
								}
								else
								{
									_this.isSaving = false;
									$(saveButton).prop('disabled', false);
								}
							});
					}
				}
			});
			
			$(cancelButton).on('mousedown', function() 
			{
				closeWindow();
			});
		}

		function saveFiles(fileMap,avpId)
		{
			var defer = new $.Deferred();
			var totalFiles = Object.keys(fileMap).length;
			var iteration = 0;
			
			if($.isEmptyObject(fileMap))
			{
				defer.resolve();
			}

			Object.keys(fileMap).forEach(function(key) 
			{
				var inputFile = fileMap[key];
				var filename = inputFile.name;
				var data = new FormData();
				var targetUrl = ACE.Salesforce.baseURL + "/services/data/v45.0/connect/files/users/" + ACE.Salesforce.userId;
				data.append("json", JSON.stringify({title: filename}));
				data.append("fileData", inputFile);

				var xhr = new XMLHttpRequest();
				xhr.open('POST', targetUrl, true);
				xhr.setRequestHeader('Authorization', 'Bearer token' + ACE.Salesforce.sessionId);

				xhr.onloadend = function(response)
				{
					// handle http request failure
					if (response.target.status != 200 && response.target.status != 201)
					{
						console.log('content version creation rest api call failed');
						return; 
					}

					var contentDocumentId = JSON.parse(response.target.response).id;

					ACE.Remoting.call("VoteInformationController.reparentFile", [contentDocumentId, avpId, null], function(result,event)
					{ 
						iteration++;
						if(totalFiles == iteration)
						{
							defer.resolve();
						}
						
						if (event.status)
						{
							console.log('update success!');
						}
						else
						{
							console.log('update failed!');
						}					
					});
				}
			
				xhr.send(data);
			});

			return defer.promise();	
		}

		var loadingModal = $('<span id="loader" class="loaderGif"/>').appendTo('body').hide();

		function closeWindow() 
		{       
			window.close();                           
		}
		
		Vote.prototype.showNotesAndContactsModal = function(canEdit, notesInputContainer, addContactsButton)
		{	
			lastVoteThis = this;

			if(this.contactIds == null)
			{
				this.contactIds = [];
			}

			modalThisNotesContainer = notesInputContainer;
			modalThisContactButton = addContactsButton;
			
			dialog.dialog('open');
			this.createNotesContactsInput(canEdit);

			return false;
		}
	});
	//# sourceURL=VoteInformation.js
})(jQuery);